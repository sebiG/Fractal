#include "program.h"

#include <stdexcept>
#include <chrono>
#include <thread>
#include <iostream>

#include <pngIO/pngIO.h>
#include <simpleVec/matrix.h>

using namespace std;
using namespace SimpleVec;

namespace LiveFractal {

Program* Program::o_instance = nullptr;

Program::Program() :
m_newton(this)
{

}

bool Program::Init() {

	// Set instance
	o_instance = this;

	// Create GLFW window
	glfwInit();

	glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
	glfwWindowHint(GLFW_FOCUSED, GLFW_TRUE);
	glfwWindowHint(GLFW_MAXIMIZED, GLFW_TRUE);
	glfwWindowHint(GLFW_RED_BITS, 8);
	glfwWindowHint(GLFW_GREEN_BITS, 8);
	glfwWindowHint(GLFW_BLUE_BITS, 8);
	glfwWindowHint(GLFW_DEPTH_BITS, 24);
	glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);
	glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
	glfwWindowHint(GLFW_CONTEXT_CREATION_API, GLFW_NATIVE_CONTEXT_API);

	o_window = glfwCreateWindow(1280, 720, "Interactive Fractals", NULL, NULL);
	if(o_window == nullptr) throw new runtime_error("glfwCreateWindow failed.");

	// Set up window event handling
	glfwSetFramebufferSizeCallback(o_window, FrameBufferSizeCallback);
	glfwSetWindowFocusCallback(o_window, FocusCallback);

	m_hasFocus = true;

	// Create Opengl context
	glfwMakeContextCurrent(o_window);
	glfwSwapInterval(1);

	// Init glew
	gl3wInit();

	// Init OpenGL
	if(!InitOpenGL()) return false;

	// Init Newton fractal
	m_newton.Init();

	// Resize framebuffer and texture
	int w, h;
	glfwGetFramebufferSize(o_window, &w, &h);
	Resize((unsigned)w, (unsigned)h);

	return true;
}

void Program::DeInit() {

	m_newton.DeInit();

	glBindTexture(GL_TEXTURE_2D, 0);
	glDeleteTextures(1, &gl_fontTexture);

	glUseProgram(0);
	glDeleteProgram(gl_program);
	glDeleteProgram(gl_textProgram);

	glBindVertexArray(0);
	glDeleteVertexArrays(1, &gl_textVertexArray);

	glfwDestroyWindow(o_window);

	glfwTerminate();

}

void Program::Run() {

	//chrono::milliseconds start = chrono::duration_cast<chrono::milliseconds> (chrono::system_clock::now().time_since_epoch());
	//chrono::milliseconds deltaTime = chrono::milliseconds(16); // Use this artificial value for the first frame

	while(!glfwWindowShouldClose(o_window)) {
		// slower fps when window is not focused
		if(!m_hasFocus) {
			this_thread::sleep_for(chrono::milliseconds(250));
		}

		m_newton.Update();
		Draw();

		glfwSwapBuffers(o_window);
		glfwPollEvents();

		/*chrono::milliseconds end = chrono::duration_cast<chrono::milliseconds> (chrono::system_clock::now().time_since_epoch());
		deltaTime = end - start;
		start = end;*/
	}

}

void Program::FrameBufferSizeCallback(GLFWwindow* window, int width, int height) {
	o_instance->Resize((unsigned)width, (unsigned)height);
}

void Program::FocusCallback(GLFWwindow* window, int focused) {
	o_instance->m_hasFocus = (focused) ? true : false;
}

void Program::Resize(unsigned width, unsigned height) {
	
	// Set new size
	m_width = width;
	m_height = height;
	m_imageSize = m_width * m_height;

	// Set text transform
	m_textTransform = 
		Matrix4_float::CreateScale(2.0f/(float)m_width, 2.0f/(float)m_height, 1.0f)
		| Matrix4_float::CreateTranslation((float)(m_width % 2)*0.5f, (float)(m_height % 2)*0.5f, 0.0f);

	// Set new viewport
	glViewport(0, 0, m_width, m_height);

	// Resize newton fractal
	m_newton.Resize(width, height);

}

void Program::Draw() {
	// Clear buffer
	const static float clearColor[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	glClearBufferfv(GL_COLOR, 0, clearColor);

	// draw texture
	uint32_t stride = m_newton.GetStride();
	if(stride != 0) {
		glUseProgram(gl_program);
		glBindVertexArray(0);
		glBindTexture(GL_TEXTURE_2D, m_newton.GetTexture());

		glUniform1ui(0, stride);
		glDrawArrays(GL_TRIANGLES, 0, 6);
	}

	// Draw HUD
	m_newton.DrawHUD();
}

bool Program::InitOpenGL() {

	// default program
	{
		GLuint vertexShader, fragmentShader;

		static GLchar const* vertexShaderSource[] = {
			"#version 430 core													\n"
			"																	\n"
			"void main() {														\n"
			"	float x = float(((uint(gl_VertexID) + 2u) / 3u)%2u);			\n"
			"	float y = float(((uint(gl_VertexID) + 1u) / 3u)%2u);			\n"
			"																	\n"
			"	gl_Position = vec4(-1.0f + x*2.0f, -1.0f + y*2.0f, 0.0f, 1.0f);	\n"
			"}																	\n"
		};

		static GLchar const* fragmentShaderSource[] = {
			"#version 430 core														\n"
			"																		\n"
			"uniform sampler2D s;													\n"
			"layout(location = 0) uniform uint stride;								\n"
			"																		\n"
			"out vec4 color;														\n"
			"																		\n"
			"void main(void) {														\n"
			"	color = vec4(texelFetch(s, ivec2(									\n"
			"		gl_FragCoord.x - uint(gl_FragCoord.x) % stride,					\n"
			"		gl_FragCoord.y - uint(gl_FragCoord.y) % stride					\n"
			"	), 0).rgb, 1.0f);													\n"
			"}																		\n"
		};

		vertexShader = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertexShader, 1, vertexShaderSource, NULL);
		glCompileShader(vertexShader);

		fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragmentShader, 1, fragmentShaderSource, NULL);
		glCompileShader(fragmentShader);

		gl_program = glCreateProgram();
		glAttachShader(gl_program, vertexShader);
		glAttachShader(gl_program, fragmentShader);
		glLinkProgram(gl_program);

		/*char buffer[1000];
		GLsizei length;
		glGetProgramInfoLog(gl_program, 1000, &length, buffer);
		cout << buffer << endl;*/

		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);
	}

	// text program

	{
		GLuint vertexShader, geometryShader, fragmentShader;

		static GLchar const* vertexShaderSource[] = {
			"#version 430 core								\n"
			"												\n"
			"layout(location = 0) in vec2 p_in;				\n"
			"layout(location = 1) in vec4 color_in;			\n"
			"layout(location = 2) in float scale_in;		\n"
			"layout(location = 3) in uint character_in;		\n"
			"												\n"
			"out VS_OUT{									\n"
			"	vec2 p;										\n"
			"	vec4 color;									\n"
			"	float scale;								\n"
			"	uint character;								\n"
			"} vs_out;										\n"
			"												\n"
			"void main() {									\n"
			"	vs_out.p = p_in;							\n"
			"	vs_out.color = color_in;					\n"
			"	vs_out.scale = scale_in;					\n"
			"	vs_out.character = character_in;			\n"
			"}												\n"
		};

		static GLchar const* geometryShaderSource[] = {
			"#version 450 core																		\n"
			"																						\n"
			"const uint textureWidth = 128;															\n"
			"const uint textureHeight = 128;														\n"
			"const uint characterWidth = 8;															\n"
			"const uint characterHeight = 8;														\n"
			"const uint charactersPerLine = 16;														\n"
			"const uint charactersPerColumn = 16;													\n"
			"																						\n"
			"layout(points) in;																		\n"
			"layout(triangle_strip, max_vertices = 4) out;											\n"
			"																						\n"
			"layout(location = 0) uniform mat4 transform;											\n"
			"																						\n"
			"in VS_OUT{																				\n"
			"	vec2 p;																				\n"
			"	vec4 color;																			\n"
			"	float scale;																		\n"
			"	uint character;																		\n"
			"} gs_in[];																				\n"
			"																						\n"
			"out vec2 uv;																			\n"
			"out vec4 color;																		\n"
			"																						\n"
			"void main() {																			\n"
			"	color = gs_in[0].color;																\n"
			"																						\n"
			"	float offset = 1.0f * gs_in[0].scale;												\n"
			"																						\n"
			"	vec2 uvBase;																		\n"
			"	uvBase.x = float(gs_in[0].character % charactersPerLine) / float(charactersPerLine);\n"
			"	uvBase.y = float(gs_in[0].character / charactersPerLine) / float(charactersPerLine);\n"
			"																						\n"
			"	float uvOffset = 1.0f / float(charactersPerLine);									\n"
			"																						\n"
			"	gl_Position = transform * vec4(gs_in[0].p + vec2(0.0f, -offset), 0.0f,	1.0f);		\n"
			"	uv = uvBase + vec2(0.0f, uvOffset);													\n"
			"	EmitVertex();																		\n"
			"																						\n"
			"	gl_Position = transform * vec4(gs_in[0].p + vec2(offset, -offset), 0.0f, 1.0f);		\n"
			"	uv = uvBase + vec2(uvOffset, uvOffset);												\n"
			"	EmitVertex();																		\n"
			"																						\n"
			"	gl_Position = transform * vec4(gs_in[0].p + vec2(0.0f, 0.0f), 0.0f, 1.0f);			\n"
			"	uv = uvBase + vec2(0.0f, 0.0f);														\n"
			"	EmitVertex();																		\n"
			"																						\n"
			"	gl_Position = transform * vec4(gs_in[0].p + vec2(offset, 0.0f), 0.0f, 1.0f);		\n"
			"	uv = uvBase + vec2(uvOffset, 0.0f);													\n"
			"	EmitVertex();																		\n"
			"}																						\n"
		};

		static GLchar const* fragmentShaderSource[] = {
			"#version 430 core												\n"
			"																\n"
			"uniform sampler2D s;											\n"
			"																\n"
			"in vec2 uv;													\n"
			"in vec4 color;													\n"
			"																\n"
			"out vec4 color_out;											\n"
			"																\n"
			"void main() {													\n"
			"	color_out = vec4(color.xyz, color.w * texture(s, uv).w);	\n"
			"}																\n"
		};

		vertexShader = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertexShader, 1, vertexShaderSource, NULL);
		glCompileShader(vertexShader);

		geometryShader = glCreateShader(GL_GEOMETRY_SHADER);
		glShaderSource(geometryShader, 1, geometryShaderSource, NULL);
		glCompileShader(geometryShader);

		fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragmentShader, 1, fragmentShaderSource, NULL);
		glCompileShader(fragmentShader);

		gl_textProgram = glCreateProgram();
		glAttachShader(gl_textProgram, vertexShader);
		glAttachShader(gl_textProgram, geometryShader);
		glAttachShader(gl_textProgram, fragmentShader);
		glLinkProgram(gl_textProgram);

		/*char buffer[1000];
		GLsizei length;
		glGetProgramInfoLog(gl_textProgram, 1000, &length, buffer);
		cout << buffer << endl;*/

		glDeleteShader(vertexShader);
		glDeleteShader(geometryShader);
		glDeleteShader(fragmentShader);

		glCreateBuffers(1, &gl_textVertexBuffer);
		glNamedBufferStorage(gl_textVertexBuffer, TEXT_DRAW_BUFFER_SIZE * sizeof(GfxChar), NULL, GL_DYNAMIC_STORAGE_BIT);

		glCreateVertexArrays(1, &gl_textVertexArray);

		glVertexArrayAttribFormat(gl_textVertexArray, 0, 2, GL_FLOAT, GL_FALSE, offsetof(GfxChar, m_pos));
		glVertexArrayAttribFormat(gl_textVertexArray, 1, 4, GL_FLOAT, GL_FALSE, offsetof(GfxChar, m_color));
		glVertexArrayAttribFormat(gl_textVertexArray, 2, 1, GL_FLOAT, GL_FALSE, offsetof(GfxChar, m_fontSize));
		glVertexArrayAttribIFormat(gl_textVertexArray, 3, 1, GL_UNSIGNED_INT, offsetof(GfxChar, m_char));

		glVertexArrayAttribBinding(gl_textVertexArray, 0, 0);
		glVertexArrayAttribBinding(gl_textVertexArray, 1, 0);
		glVertexArrayAttribBinding(gl_textVertexArray, 2, 0);
		glVertexArrayAttribBinding(gl_textVertexArray, 3, 0);

		glVertexArrayVertexBuffer(gl_textVertexArray, 0, gl_textVertexBuffer, 0, sizeof(GfxChar));

		glCreateTextures(GL_TEXTURE_2D, 1, &gl_fontTexture);

		ifstream is("font.png", ios::binary);
		if(!is.is_open()) {
			cout << "Cannot open font file 'font.png'." << endl;
			return false;
		}

		uint32_t channels, bitDepth;
		int colorType;
		unsigned char* data;

		try {
			PngIO::Read(is, &m_fontTextureWidth, &m_fontTextureHeight, &colorType, &channels, &bitDepth, &data);
		} catch(runtime_error const& e) {
			cout << "Error while reading font file 'font.png': " << e.what() << endl;
			return false;
		}

		if(channels != 4 || bitDepth != 8) {
			cout << "Unsopported pixel format in font file 'font.png'.";
			return false;
		}

		glTextureStorage2D(gl_fontTexture, 1, GL_RGBA32F, m_fontTextureWidth, m_fontTextureHeight);
		glTextureSubImage2D(gl_fontTexture, 0, 0, 0, m_fontTextureWidth, m_fontTextureHeight, GL_RGBA, GL_UNSIGNED_BYTE, data);
		delete[] data;

		glTextureParameteri(gl_fontTexture, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTextureParameteri(gl_fontTexture, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTextureParameteri(gl_fontTexture, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTextureParameteri(gl_fontTexture, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	}

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	m_textBufferSize = 0;

	return true;

}

void Program::DrawCharacters(GfxChar const* characters, uint32_t count) {
	while(count > 0) {
		uint32_t drawCount = min(count, TEXT_DRAW_BUFFER_SIZE - m_textBufferSize);
		if(m_textBufferSize >= TEXT_DRAW_BUFFER_SIZE) {
			FlushCharacters();
		}
		memcpy(&m_textBuffer[m_textBufferSize], characters, drawCount * sizeof(GfxChar));
		m_textBufferSize += drawCount;
		count -= drawCount;
	}
}

void Program::DrawCharacterString(
	string const& text,
	Vector2_float pos,
	Vector4_float color,
	float fontSize,
	Vector2_float fontSpacing
) {
	GfxChar* chars = new GfxChar[text.length()];
	uint32_t actualSize = 0;

	Vector2_float offset = { 0.0f, 0.0f };
	for(char i : text) {
		// newlines
		if(i == '\n') {
			offset.x = 0.0f;
			offset.y -= fontSize * fontSpacing.y;
			++actualSize;
			continue;
		}

		// tabs
		if(i == '\t') {
			offset.x += 3.0f * fontSize * fontSpacing.x;
			++actualSize;
			continue;
		}

		// normal characters
		chars[actualSize].m_pos = pos + offset;
		chars[actualSize].m_color = color;
		chars[actualSize].m_fontSize = fontSize;
		chars[actualSize].m_char = i;

		offset.x += fontSize * fontSpacing.x;
		++actualSize;
	}

	DrawCharacters(chars, actualSize);
	delete[] chars;
}

void Program::FlushCharacters() {
	glFlush();

	glNamedBufferSubData(gl_textVertexBuffer, 0, m_textBufferSize * sizeof(GfxChar), m_textBuffer);

	glUseProgram(gl_textProgram);

	glBindTexture(GL_TEXTURE_2D, gl_fontTexture);

	glEnableVertexArrayAttrib(gl_textVertexArray, 0);
	glEnableVertexArrayAttrib(gl_textVertexArray, 1);
	glEnableVertexArrayAttrib(gl_textVertexArray, 2);
	glEnableVertexArrayAttrib(gl_textVertexArray, 3);

	glUniformMatrix4fv(0, 1, GL_FALSE, (GLfloat*)m_textTransform);

	glBindVertexArray(gl_textVertexArray);
	glDrawArrays(GL_POINTS, 0, m_textBufferSize);

	m_textBufferSize = 0;
}

}
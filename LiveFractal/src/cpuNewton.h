#ifndef LIVEFRACTAL_CPU_NEWTON_H
#define LIVEFRACTAL_CPU_NEWTON_H

#include "standard.h"

#include <complex>
#include <random>
#include <thread>
#include <atomic>
#include <mutex>
#include <fstream>

#include <simpleVec/vector.h>
#include <colorCvt/colorCvt.h>

namespace LiveFractal {

class Program;

class CPUNewton {

public:

	CPUNewton(Program* program);
	~CPUNewton() = default;

	void Init();
	void DeInit();

	void Update();
	void DrawHUD();
	void Resize(uint32_t width, uint32_t height);
	void Redraw() { m_redraw = true; }

	GLuint GetTexture() const { return gl_texture; }
	uint32_t GetStride() const { return m_stride; }

	const static uint32_t MAX_POLY_ORDER = 15;

private:

	const static double ZERO_EPSILON;
	const static uint32_t INITIAL_ITERATIONS = 100;

	static void ThreadFunction(CPUNewton* n, uint32_t i);

	std::complex<double> EvalRatio(
		std::complex<double>* evalBuffer,
		std::complex<double>* coeffs,
		uint32_t order,
		std::complex<double> value
	);
	void FactorZero(
		std::complex<double>* coeffs,
		uint32_t order,
		std::complex<double> zero,
		std::complex<double>* resCoeffs
	);
	uint32_t NewtonMethod(
		std::complex<double>* evalBuffer,
		std::complex<double>* coeffs,
		uint32_t order,
		uint32_t maxIter,
		std::complex<double> value,
		double convergence,
		std::complex<double>* resZero
	);
	void FindZerosRandomly();
	void FindNewZeros();

	// ----------------------------------- //

	Program* o_program;

	std::complex<double> m_coeffs[MAX_POLY_ORDER + 1];
	std::complex<double> m_zeros[MAX_POLY_ORDER + 1];
	ColorCvt::HSV<float> m_colors[MAX_POLY_ORDER + 2];
	uint32_t m_order;

	uint32_t m_width;
	uint32_t m_height;

	std::complex<double> m_fractalCenter;
	double m_fractalWidth;
	double m_pixelWidth;
	uint32_t m_maxIterations;

	enum ThreadState {
		RUN,
		IDLE,
		IDLING
	};

	const static uint32_t THREAD_COUNT = 8;
	std::atomic_bool m_stopRequired;
	std::thread m_threads[THREAD_COUNT];
	std::atomic<ThreadState> m_threadState[THREAD_COUNT];
	std::atomic_uint32_t m_currentPixel[THREAD_COUNT];
	std::atomic_uint32_t m_nextPixel;
	uint32_t* p_columnMap = nullptr;
	uint32_t* p_strideMap = nullptr;
	uint32_t m_strideCount;
	std::atomic_uint32_t m_currentStrideIndex;
	uint32_t m_stride;

	ColorCvt::RGB<float>* p_data = nullptr;
	bool m_redraw;

	GLuint gl_texture;

	std::mt19937 m_randomGenerator;
	std::uniform_real_distribution<double> m_randomDistribution;

	const static double COEFF_CHANGE_SPEED;
	const static double MOVE_SPEED_MULTIPLIER;
	const static double ZOOM_FACTOR;
	uint32_t m_selectedCoeff;
	bool m_tabPressed;

	uint32_t m_frameCount;
	std::ofstream m_recording;
};

}

#endif
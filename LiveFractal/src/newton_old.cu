#include "newton_old.h"

#include "program.h"

#include <algorithm>
#include <iostream>

using namespace std;
using namespace SimpleVec;

namespace LiveFractal {

const static uint32_t BUFFER_SIZE = 1024 * 1024;
const static uint32_t COPY_KERNEL_THREAD_COUNT = 128;
const static uint32_t COLUMN_KERNEL_THREAD_COUNT = 128;

__device__ float HSVRGB_f(float n, float3 const& hsv) {
	float k = fmodf((n + hsv.x/60.0f), 6.0f);
	return hsv.z - hsv.z * hsv.y * max(min(k, min(4.0f - k, 1.0f)), 0.0f);
}

__device__ float3 HSVtoRGB(float3 const& hsv) {
	return make_float3(HSVRGB_f(1.0f, hsv), HSVRGB_f(3.0f, hsv), HSVRGB_f(1.0f, hsv));
}

__global__ void CopyKernel(
	cudaSurfaceObject_t s,
	uint32_t width,
	uint32_t height,
	uint32_t** data,
	float3* colors,
	float maxIterations
) {

	uint32_t pixel = blockIdx.x * blockDim.x + threadIdx.x;
	if(pixel >= width * height) return;

	uint32_t bufferIndex = pixel / (BUFFER_SIZE * 2);
	uint32_t pixelIndex = pixel % (BUFFER_SIZE * 2);
	uint32_t x = pixel % width;
	uint32_t y = pixel / width;

	float3 hsv;
	float3 rgb;

	uint32_t colorIndex = data[bufferIndex][pixelIndex * 2];
	hsv.x = colors[colorIndex].x;
	hsv.y = colors[colorIndex].y;
	hsv.z = colors[colorIndex].z;
	hsv.z *= 1.0f - (float)data[bufferIndex][pixelIndex * 2 + 1] / maxIterations;
	hsv.z = (float)data[bufferIndex][pixelIndex * 2 + 1] / maxIterations;
	
	rgb = HSVtoRGB(hsv);
	//float4 test = make_float4((float)x / (float)width, (float)y / (float)height, 1.0f, 1.0f);
	surf2Dwrite(make_float4(rgb.x, rgb.y, rgb.z, 1.0f), s, x * sizeof(float4), y);
}

__device__ void EvalPoly(
	double* coeffs,
	uint32_t order,
	double zre,
	double zim,
	double* fre,
	double* fim,
	double* f_re,
	double* f_im
) {
	double evals[2*Newton::MAX_POLY_ORDER + 2];

	evals[0] = 1.0;
	evals[1] = 0.0;

	*fre = coeffs[0];
	*fim = coeffs[1];
	*f_re = coeffs[2];
	*f_im = coeffs[3];

	for(uint32_t i = 1; i <= order - 1; i++) {
		evals[2*i] = zre*evals[2*i-2] - zim*evals[2*i-1];
		evals[2*i+1] = zre*evals[2*i-1] + zim*evals[2*i-2];

		*fre += coeffs[2*i]*evals[2*i] - coeffs[2*i+1]*evals[2*i+1];
		*fim += coeffs[2*i]*evals[2*i+1] + coeffs[2*i+1]*evals[2*i];
		if(i < order) {
			*f_re += ((double)i + 1.0) * (coeffs[2*i+2]*evals[2*i] - coeffs[2*i+3]*evals[2*i+1]);
			*f_im += ((double)i + 1.0) * (coeffs[2*i+2]*evals[2*i+1] + coeffs[2*i+3]*evals[2*i]);
		}
	}
}

__global__ void ColumnKernel(
	uint32_t column,
	uint32_t width,
	uint32_t height,
	uint32_t maxIterations,
	double* coeffs,
	uint32_t order,
	double* zeros,
	double reCenter,
	double imCenter,
	double pixelWidth,
	uint32_t** data
) {

	uint32_t row = blockIdx.x * blockDim.x + threadIdx.x;
	if(row >= height) return;

	uint32_t pixelIndex = row * width + column;
	uint32_t bufferIndex = pixelIndex / (BUFFER_SIZE * 2);
	pixelIndex %= (BUFFER_SIZE * 2);

	double re = reCenter + pixelWidth * ((double)column - (double)width / 2.0);
	double im = imCenter + pixelWidth * ((double)row - (double)height / 2.0);

	double oldRe;
	double oldIm;

	for(uint32_t i = 0; i < maxIterations; ++i) {
		oldRe = re;
		oldIm = im;

		double fre, fim, f_re, f_im;
		EvalPoly(coeffs, order, re, im, &fre, &fim, &f_re, &f_im);

		double cd = f_re*f_re + f_im*f_im;
		re -= (fre*f_re + fim*f_im) / cd;
		im -= (fim*f_re - fre*f_im) / cd;

		if((re-oldRe)*(re-oldRe) + (im-oldIm)*(im-oldIm) < pixelWidth*pixelWidth) {
			data[bufferIndex][pixelIndex + 1] = i;
			for(uint32_t j = 0; j < order; j++) {
				if((zeros[j*2]-re)*(zeros[j*2]-re) + (zeros[j*2+1]-im)*(zeros[j*2+1]-im) < 10.0*pixelWidth*pixelWidth) {
					data[bufferIndex][pixelIndex] = j + 1;
					return;
				}
			}
			data[bufferIndex][pixelIndex] = 0;
		}
	}
}

Newton::Newton(Program* program) :
	o_program(program) {
}

void Newton::Init() {
	// set up polynomial
	m_order = 0;
	for(uint32_t i = 0; i <= MAX_POLY_ORDER; i++) m_coeffs[i] = 0;

	// set up random generator
	m_randomGenerator = mt19937(random_device{}());

	// set up cuda
	cudaSetDevice(1);

	cudaError_t error = cudaSuccess;
	error = cudaStreamCreate(&cu_computeStream);
	if(error != cudaSuccess) throw runtime_error("cudaStreamCreate failed.");
	error = cudaStreamCreate(&cu_copyStream);
	if(error != cudaSuccess) throw runtime_error("cudaStreamCreate failed.");

	// set up thread
	m_dataResizeRequired = false;
	m_redrawRequired = false;
	m_stopRequired = false;
	m_stride = 0;

	// start thread
	m_thread = thread([this]() {
		Newton::ThreadFunction(this);
	});

	// test polynomial
	m_coeffs[3] = 1;
	m_coeffs[2] = -6;
	m_coeffs[1] = 11;
	m_coeffs[0] = -6;
	m_order = 3;

	FindAllZeros();

	// test settings
	m_fractalCenter = 0.0;
	m_fractalWidth = 5.0;
	m_maxIterations = 20;

	// test colors
	m_colors[0] = { 0.0f, 0.0f, 0.0f };
	m_colors[1] = { 0.0f, 1.0f, 1.0f };
	m_colors[2] = { 120.0f, 1.0f, 1.0f };
	m_colors[3] = { 240.0f, 1.0f, 1.0f };

	// set up coefficients
	error = cudaMalloc((void**)&dp_coeffs, (MAX_POLY_ORDER + 1) * 2 * sizeof(double));
	if(error != cudaSuccess) throw runtime_error("cudaMalloc failed: cannot allocate memory for coefficients.");

	error = cudaMemcpy(dp_coeffs, m_coeffs, (MAX_POLY_ORDER + 1) * 2 * sizeof(double), cudaMemcpyHostToDevice);
	if(error != cudaSuccess) throw runtime_error("cudaMemcpy failed.");

	// set up zeros
	error = cudaMalloc((void**)&dp_zeros, (MAX_POLY_ORDER + 1) * 2 * sizeof(double));
	if(error != cudaSuccess) throw runtime_error("cudaMalloc failed: cannot allocate memory for zeros.");

	error = cudaMemcpy(dp_zeros, m_zeros, (MAX_POLY_ORDER + 1) * 2 * sizeof(double), cudaMemcpyHostToDevice);
	if(error != cudaSuccess) throw runtime_error("cudaMemcpy failed.");

	// set up colors
	error = cudaMalloc((void**)&dp_colors, (MAX_POLY_ORDER + 1) * sizeof(float3));
	if(error != cudaSuccess) throw runtime_error("cudaMalloc failed: cannot allocate memory for colors.");

	error = cudaMemcpy(dp_colors, m_colors, (MAX_POLY_ORDER + 1) * sizeof(float3), cudaMemcpyHostToDevice);
	if(error != cudaSuccess) throw runtime_error("cudaMemcpy failed.");

}

void Newton::RequestStop() {
	m_stopRequired = true;
}

void Newton::Join() {
	if(m_thread.joinable()) m_thread.join();
}

void Newton::Update() {
	cudaError_t error = cudaSuccess;

	if(p_dataPointers != nullptr && !m_dataResizeRequired) {

		lock_guard<mutex> lock(m_dataMutex);

		// map resource
		cudaArray_t array;

		error = cudaGraphicsMapResources(1, &this->cu_textureResource);
		if(error != cudaSuccess) throw runtime_error("cudaGraphicsMapResources failed.");
		error = cudaGraphicsSubResourceGetMappedArray(&array, this->cu_textureResource, 0, 0);
		if(error != cudaSuccess) throw runtime_error("cudaGraphicsSubResourceGetMappedArray failed.");

		// get surface
		cudaResourceDesc resDesc;
		memset(&resDesc, 0, sizeof(cudaResourceDesc));
		resDesc.resType = cudaResourceTypeArray;
		resDesc.res.array.array = array;

		cudaSurfaceObject_t s;
		error = cudaCreateSurfaceObject(&s, &resDesc);
		if(error != cudaSuccess) throw runtime_error("cudaCreateSurfaceObject failed.");

		// copy kernel
		uint32_t blocks = m_width * m_height / COPY_KERNEL_THREAD_COUNT + (m_width * m_height % COPY_KERNEL_THREAD_COUNT != 0);
		CopyKernel<<<blocks, COPY_KERNEL_THREAD_COUNT, 0, cu_copyStream>>> (s, m_width, m_height, dp_dataPointers, dp_colors, (float)m_maxIterations);

		error = cudaStreamSynchronize(cu_copyStream);
		if(error != cudaSuccess) {
			throw runtime_error("cudaStreamSynchronize failed.");
		}

		// destroy surface
		error = cudaDestroySurfaceObject(s);
		if(error != cudaSuccess) throw runtime_error("cudaDestroySurfaceObject failed.");

		// unmap resource
		error = cudaGraphicsUnmapResources(1, &this->cu_textureResource);
		if(error != cudaSuccess) throw runtime_error("cudaGraphicsUnmapResources failed.");

	}
}

void Newton::DrawHUD() {
	stringstream polyString;
	for(int32_t i = m_order; i >= 0; i--) {
		polyString << m_coeffs[i] << "x^" << i << " ";
	}

	Vector2_float pos = {
		-float(o_program->GetWindowWidth()) / 2.0f + 4.0f,
		-float(o_program->GetWindowHeight()) / 2.0f + 20.0f
	};
	
	o_program->DrawCharacterString(
		polyString.str(),
		pos + Vector2_float{2.0f, 2.0f},
		{ 0.0f, 0.0f, 0.0f, 1.0f },
		16.0f,
		{ 6.0f/8.0f, 1.0f }
	);
	o_program->DrawCharacterString(
		polyString.str(),
		pos,
		{ 1.0f, 1.0f, 1.0f, 1.0f },
		16.0f,
		{6.0f/8.0f, 1.0f}
	);
	o_program->FlushCharacters();
}

void Newton::Resize(uint32_t width, uint32_t height) {
	m_width = width;
	m_height = height;

	// Delete old texture
	glBindTexture(GL_TEXTURE_2D, 0);
	glDeleteTextures(1, &gl_texture);

	// Create new texture
	glCreateTextures(GL_TEXTURE_2D, 1, &gl_texture);
	glTextureStorage2D(gl_texture, 1, GL_RGBA32F, m_width, m_height);
	glBindTexture(GL_TEXTURE_2D, gl_texture);

	GLfloat* data = new GLfloat[4 * width * height];
	for(unsigned y = 0; y < m_height; y++) {
		for(unsigned x = 0; x < m_width; x++) {
			// test values
			data[4 * (y * m_width + x)] = 0.0f;// (float)x / (float)m_width;
			data[4 * (y * m_width + x) + 1] = 0.0f;// (float)y / (float)m_height;
			data[4 * (y * m_width + x) + 2] = 0.0f;
			data[1 * (y * m_width + x) + 3] = 1.0f;
		}
	}
	glTextureSubImage2D(gl_texture, 0, 0, 0, m_width, m_height, GL_RGBA, GL_FLOAT, data);
	delete[] data;

	cudaError error;
	error = cudaGraphicsGLRegisterImage(&cu_textureResource, gl_texture, GL_TEXTURE_2D, cudaGraphicsRegisterFlagsSurfaceLoadStore);
	if(error != cudaSuccess) {
		throw runtime_error("cudaGraphicsGLRegisterImage failed.");
	}

	// require buffer resize
	m_dataResizeRequired = true;
}

void Newton::Redraw() {
	m_redrawRequired = true;
}

void Newton::ThreadFunction(Newton* newton) {
	cudaError error = cudaSuccess;

	cudaSetDevice(1);

	bool drawRequired = false;

	while(!newton->m_stopRequired) {
		if(newton->m_dataResizeRequired) {
			// resize data buffer
			newton->ResizeDataBuffer();
			newton->m_dataResizeRequired = false;
			drawRequired = true;
		}

		if(newton->m_redrawRequired) {
			newton->m_redrawRequired = false;
			drawRequired = true;
		}

		uint32_t blocks = newton->m_height / COLUMN_KERNEL_THREAD_COUNT + (newton->m_height % COLUMN_KERNEL_THREAD_COUNT != 0);

		if(drawRequired) {
			// draw first column
			{
				ColumnKernel<<<blocks, COLUMN_KERNEL_THREAD_COUNT, 0, newton->cu_computeStream>>>(
					0,
					newton->m_width,
					newton->m_height,
					newton->m_maxIterations,
					newton->dp_coeffs,
					newton->m_order,
					newton->dp_zeros,
					newton->m_fractalCenter.real(),
					newton->m_fractalCenter.imag(),
					newton->m_fractalWidth / (double)newton->m_width,
					newton->dp_dataPointers
					);
				error = cudaStreamSynchronize(newton->cu_computeStream);
				if(error != cudaSuccess) {
					throw runtime_error("cudaStreamSynchronize failed: Could not sxnchronize with compute stream.");
				}
			}

			// find initial stride
			{
				uint32_t pow = (uint32_t)ceil(log(newton->m_width)/log(2.0));
				newton->m_stride = 1 << pow;
			}

			for(; newton->m_stride > 1; newton->m_stride = newton->m_stride / 2) {
				for(
					uint32_t i = newton->m_stride / 2;
					i < newton->m_width;
					i += newton->m_stride
				) {
					if(newton->m_stopRequired || newton->m_dataResizeRequired || newton->m_redrawRequired) goto endDrawing;

					// draw column
					{
						ColumnKernel<<<blocks, COLUMN_KERNEL_THREAD_COUNT, 0, newton->cu_computeStream>>>(
							i,
							newton->m_width,
							newton->m_height,
							newton->m_maxIterations,
							newton->dp_coeffs,
							newton->m_order,
							newton->dp_zeros,
							newton->m_fractalCenter.real(),
							newton->m_fractalCenter.imag(),
							newton->m_fractalWidth / (double)newton->m_width,
							newton->dp_dataPointers
							);
						error = cudaStreamSynchronize(newton->cu_computeStream);
						if(error != cudaSuccess) {
							throw runtime_error("cudaStreamSynchronize failed: Could not synchronize with compute stream.");
						}
					}
				}
			}

			drawRequired = false;
		endDrawing:;
		}
	}
}

void Newton::ResizeDataBuffer() {
	cudaError error = cudaSuccess;

	lock_guard<mutex> lock(m_dataMutex);

	// delete old buffer
	if(p_dataPointers != nullptr) {
		for(uint32_t i = 0; i < m_dataPointersCount; ++i) {
			error = cudaFree(p_dataPointers[i]);
			if(error != cudaSuccess) throw runtime_error("cudaFree failed: cannot free buffer data.");
		}
		error = cudaFree(dp_dataPointers);
		if(error != cudaSuccess) throw runtime_error("cudaFree failed: cannot free buffer data pointers.");
		delete[] p_dataPointers;
	}

	// allocate new buffer
	m_dataPointersCount = m_width * m_height * 2 / BUFFER_SIZE + (m_width * m_height * 2 % BUFFER_SIZE != 0);
	error = cudaMalloc((void**)&dp_dataPointers, m_dataPointersCount * sizeof(uint32_t*));
	if(error != cudaSuccess) throw runtime_error("cudaMalloc failed: cannot allocate buffer data pointers.");
	p_dataPointers = new uint32_t*[m_dataPointersCount];

	for(uint32_t i = 0; i < m_dataPointersCount; ++i) {
		error = cudaMalloc((void**)&p_dataPointers[i], BUFFER_SIZE * sizeof(uint32_t));
		if(error != cudaSuccess) throw runtime_error("cudaMalloc failed: cannot allocate buffer data.");
		error = cudaMemset((void*)p_dataPointers[i], 0, BUFFER_SIZE * sizeof(uint32_t));
		if(error != cudaSuccess) throw runtime_error("cudaMemset failed: cannot set default buffer data.");
	}

	error = cudaMemcpy(dp_dataPointers, p_dataPointers, m_dataPointersCount * sizeof(uint32_t), cudaMemcpyHostToDevice);
	if(error != cudaSuccess) throw runtime_error("cudaMemcpy failed: cannot copy data pointers to device.");

	error = cudaStreamSynchronize(cu_copyStream);
	if(error != cudaSuccess) {
		throw runtime_error("cudaStreamSynchronize failed.");
	}
	
}

complex<double> Newton::EvalPoly(
	std::complex<double>* coeffs,
	uint32_t order,
	complex<double> value
) {
	complex<double> evals[MAX_POLY_ORDER + 1];
	std::complex<double> ret;
	evals[0] = 1.0;
	ret = coeffs[0];
	for(uint32_t i = 1; i <= order; i++) {
		evals[i] = evals[i - 1] * value;
		ret += coeffs[i] * evals[i];
	}
	return ret;
}

std::complex<double> Newton::EvalDerivPoly(
	std::complex<double>* coeffs,
	uint32_t order,
	std::complex<double> value
) {
	if(order == 0) return 0.0;
	complex<double> evals[MAX_POLY_ORDER - 1];
	std::complex<double> ret;
	evals[0] = 1.0;
	ret = coeffs[1];
	for(uint32_t i = 1; i <= order - 1; i++) {
		evals[i] = evals[i - 1] * value;
		ret += complex<double>(i + 1, 0) * coeffs[i + 1] * evals[i];
	}
	return ret;
}

void Newton::FactorZero(
	complex<double>* coeffs,
	uint32_t order,
	complex<double> zero,
	complex<double>* resCoeffs
) {
	if(order == 0) return;
	complex<double> diff = 0.0;
	for(int32_t i = order - 1; i >= 0; i--) {
		resCoeffs[i] = coeffs[i + 1] - diff;
		diff = -zero * resCoeffs[i];
	}
}

bool Newton::CPU_NewtonMethod(
	std::complex<double>* coeffs,
	uint32_t order,
	uint32_t maxIter,
	std::complex<double> value,
	double convergence,
	std::complex<double>* resZero
) {
	if(order == 0) return false;
	if(order == 1) {
		*resZero = -coeffs[0] / coeffs[1];
		return true;
	}

	complex<double> old = 100000.0; // far away

	convergence *= convergence; // check against square instead

	for(uint32_t i = 0; i < maxIter; i++) {
		old = value;
		value = value - EvalPoly(coeffs, order, value) / EvalDerivPoly(coeffs, order, value);
		if(norm(value - old) < convergence) {
			*resZero = value;
			return true;
		}
	}

	return false;
}

void Newton::FindAllZeros() {
	std::complex<double> coeffs1[MAX_POLY_ORDER + 1];
	std::complex<double> coeffs2[MAX_POLY_ORDER];

	std::complex<double>* p = coeffs1;
	std::complex<double>* q = coeffs2;

	memcpy(p, m_coeffs, sizeof(complex<double>) * (MAX_POLY_ORDER + 1));

	for(uint32_t i = 0; i < m_order; i++) {

		if(i > 0) {
			FactorZero(
				p,
				m_order - i + 1,
				m_zeros[i - 1],
				q
			);
			swap(p, q);
		}

		while(true) {
			double re = m_randomDistribution(
				m_randomGenerator,
				uniform_real_distribution<double>::param_type{ 0, 1 }
			);
			double im = m_randomDistribution(
				m_randomGenerator,
				uniform_real_distribution<double>::param_type{ 0, 1 }
			);
			if(CPU_NewtonMethod(
				p,
				m_order - i,
				100,
				{ re, im },
				0.01,
				&m_zeros[i]
			)) break;
		}
	}
}

}
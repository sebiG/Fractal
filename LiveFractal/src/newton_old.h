#ifndef LIVEFRACTAL_NEWTON_H
#define LIVEFRACTAL_NEWTON_H

#include "standard.h"

#include <complex>
#include <random>
#include <thread>
#include <atomic>
#include <mutex>

#include <simpleVec/vector.h>
#include <colorCvt/colorCvt.h>

namespace LiveFractal {

class Program;

class Newton {

public:

	Newton(Program* program);
	~Newton() = default;

	const static uint32_t MAX_POLY_ORDER = 15;

	void Init();
	void RequestStop();
	void Join();

	void Update();
	void DrawHUD();
	void Resize(uint32_t width, uint32_t height);
	void Redraw();

	GLuint GetTexture() const { return gl_texture; }
	uint32_t GetStride() const { return m_stride; }

private:

	static void ThreadFunction(Newton* newton);

	void ResizeDataBuffer();

	std::complex<double> EvalPoly(
		std::complex<double>* coeffs,
		uint32_t order,
		std::complex<double> value
	);
	std::complex<double> EvalDerivPoly(
		std::complex<double>* coeffs,
		uint32_t order,
		std::complex<double> value
	);
	void FactorZero(
		std::complex<double>* coeffs,
		uint32_t order,
		std::complex<double> zero,
		std::complex<double>* resCoeffs
	);
	bool CPU_NewtonMethod(
		std::complex<double>* coeffs,
		uint32_t order,
		uint32_t maxIter,
		std::complex<double> value,
		double convergence,
		std::complex<double>* resZero
	);
	void FindAllZeros();

	void DrawFractalGrid(SimpleVec::Vector2<uint32_t> offset);

	// ----------------------------------- //

	Program* o_program;

	std::thread m_thread;

	// ----------------------------------- //

	std::complex<double> m_coeffs[MAX_POLY_ORDER + 1];
	uint32_t m_order;

	std::complex<double> m_zeros[MAX_POLY_ORDER + 1];

	std::complex<double> m_fractalCenter;
	double m_fractalWidth;
	uint32_t m_maxIterations;

	ColorCvt::HSV<float> m_colors[MAX_POLY_ORDER + 1];

	// ----------------------------------- //

	std::mt19937 m_randomGenerator;
	std::uniform_real_distribution<double> m_randomDistribution;

	// ----------------------------------- // 

	std::atomic_bool m_dataResizeRequired;
	std::atomic_bool m_redrawRequired;
	std::mutex m_dataMutex;

	std::atomic_bool m_stopRequired;

	std::atomic_uint32_t m_stride;
	uint32_t m_width;
	uint32_t m_height;

	cudaStream_t cu_computeStream;
	cudaStream_t cu_copyStream;

	GLuint gl_texture;
	cudaGraphicsResource* cu_textureResource;

	uint32_t** dp_dataPointers = nullptr;
	uint32_t** p_dataPointers = nullptr;
	uint32_t m_dataPointersCount = 0;
	double* dp_coeffs = nullptr;
	double* dp_zeros = nullptr;
	float3* dp_colors = nullptr;
};

}

#endif
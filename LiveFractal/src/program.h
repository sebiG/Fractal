#ifndef LIVEFRACTAL_PROGRAM_H
#define LIVEFRACTAL_PROGRAM_H

#include "standard.h"

#include "cpuNewton.h"

#include <string>

#include <GLFW/glfw3.h>
#include <simpleVec/vector.h>
#include <simpleVec/matrix.h>

namespace LiveFractal {

class Program {

public:

	Program();
	~Program() = default;

	bool Init();
	void DeInit();
	void Run();

	// ----------------------------------- //

	uint32_t GetWindowWidth() const { return m_width; }
	uint32_t GetWindowHeight() const { return m_height; }

	//cudaGraphicsResource* GetTextureResource() const { return cu_textureResource; }

	// ----------------------------------- //

	struct GfxChar {
		SimpleVec::Vector2<float> m_pos;
		SimpleVec::Vector4<float> m_color;
		float m_fontSize;
		uint32_t m_char;
	};

	void DrawCharacters(GfxChar const* characters, uint32_t count);
	void DrawCharacterString(
		std::string const& text,
		SimpleVec::Vector2_float pos,
		SimpleVec::Vector4_float color,
		float fontSize,
		SimpleVec::Vector2_float fontSpacing = { 1.0f, 1.0f }
	);
	void FlushCharacters();

private:

	static void FrameBufferSizeCallback(GLFWwindow* window, int width, int height);
	static void FocusCallback(GLFWwindow* window, int focused);

	void Resize(unsigned width, unsigned height);
	void Draw();

	bool InitOpenGL();

	// ----------------------------------- //

	static Program* o_instance;

	GLFWwindow* o_window;
	bool m_hasFocus;

	bool m_needsResize;

	// ----------------------------------- //

	CPUNewton m_newton;

	unsigned m_width;
	unsigned m_height;
	unsigned m_imageSize;

	GLuint gl_program;
	GLuint gl_texture;

	//cudaGraphicsResource* cu_textureResource;

	GLuint gl_textProgram;
	GLuint gl_textVertexArray;
	GLuint gl_textVertexBuffer;
	GLuint gl_fontTexture;
	uint32_t m_fontTextureWidth;
	uint32_t m_fontTextureHeight;
	SimpleVec::Matrix4_float m_textTransform;

	const static uint32_t TEXT_DRAW_BUFFER_SIZE = 128;
	GfxChar m_textBuffer[TEXT_DRAW_BUFFER_SIZE];
	uint32_t m_textBufferSize;

};

}

#endif

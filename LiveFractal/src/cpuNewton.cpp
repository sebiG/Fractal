#include "cpuNewton.h"

#include "program.h"

#include <algorithm>
#include <iostream>

using namespace std;
using namespace SimpleVec;

namespace LiveFractal {

const double CPUNewton::ZERO_EPSILON = 1e-2;
const double CPUNewton::COEFF_CHANGE_SPEED = 0.05;
const double CPUNewton::MOVE_SPEED_MULTIPLIER = 0.02;
const double CPUNewton::ZOOM_FACTOR = 1.01;

CPUNewton::CPUNewton(Program* program) :
	o_program(program) {
}

void CPUNewton::Init() {
	// set up polynomial
	m_order = 0;
	for(uint32_t i = 0; i <= MAX_POLY_ORDER; i++) m_coeffs[i] = 0;

	// set up random generator
	m_randomGenerator = mt19937(random_device{}());

	// set up threads
	m_stopRequired = false;
	for(uint32_t i = 0; i < THREAD_COUNT; ++i) {
		m_threadState[i] = ThreadState::IDLING;
		m_threads[i] = thread(ThreadFunction, this, i);
	}

	// interaction
	m_selectedCoeff = 0;

	// test polynomial and colors
	m_colors[0] = { 0.0f, 0.0f, 0.0f };
	/*for(uint32_t i = 0; i <= 15; i++) {
		m_coeffs[i] = 1.0;
		m_colors[i + 1] = { 120.0f * (float)i / 15.0f, 1.0f, 1.0f };
	}*/
	m_colors[1] = { 0.0f, 1.0f, 1.0f };
	m_colors[2] = { 12.0f, 0.753f, 0.698f };
	m_colors[3] = { 171.0f, 0.251f, 0.671f };
	m_colors[4] = { 30.0f, 0.032f, 0.988f };
	m_colors[5] = { 36.0f, 0.571f, 0.859f };

	m_coeffs[0] = -1.0;
	m_coeffs[4] = 1.0;

	m_order = 4;

	FindZerosRandomly();

	// test settings
	m_fractalCenter = 1.5;
	m_fractalWidth = 5.0;
	m_maxIterations = 30;

	// test recording
	m_frameCount = 0;
	m_recording.open("rec.txt");

}

void CPUNewton::DeInit() {
	m_stopRequired = true;
	for(uint32_t i = 0; i < THREAD_COUNT; ++i) {
		if(m_threads[i].joinable()) m_threads[i].join();
	}
	m_recording.close();
	delete[] p_data;
}

void CPUNewton::Update() {
	// interaction
	bool polyChanged = false;
	bool redraw = false;
	if(GetKeyState('W') & 0x8000) {
		m_coeffs[m_selectedCoeff] += COEFF_CHANGE_SPEED;
		polyChanged = true;
		redraw = true;
	}
	if(GetKeyState('S') & 0x8000) {
		m_coeffs[m_selectedCoeff] -= COEFF_CHANGE_SPEED;
		polyChanged = true;
		redraw = true;
	}
	if(GetKeyState(VK_UP) & 0x8000) {
		m_fractalCenter += complex<double>{ 0.0, m_fractalWidth * MOVE_SPEED_MULTIPLIER };
		redraw = true;
	}
	if(GetKeyState(VK_DOWN) & 0x8000) {
		m_fractalCenter += complex<double>{ 0.0, -m_fractalWidth * MOVE_SPEED_MULTIPLIER };
		redraw = true;
	}
	if(GetKeyState(VK_RIGHT) & 0x8000) {
		m_fractalCenter += complex<double>{ m_fractalWidth * MOVE_SPEED_MULTIPLIER, 0.0 };
		redraw = true;
	}
	if(GetKeyState(VK_LEFT) & 0x8000) {
		m_fractalCenter += complex<double>{ -m_fractalWidth * MOVE_SPEED_MULTIPLIER, 0.0 };
		redraw = true;
	}
	if(GetKeyState('E') & 0x8000) {
		m_fractalWidth /= ZOOM_FACTOR;
		redraw = true;
	}
	if(GetKeyState('D') & 0x8000) {
		m_fractalWidth *= ZOOM_FACTOR;
		redraw = true;
	}
	if(GetKeyState(VK_TAB) & 0x8000) {
		if(!m_tabPressed) {
			m_tabPressed = true;
			++m_selectedCoeff;
			if(m_selectedCoeff > m_order) m_selectedCoeff = 0;
		}
	} else {
		m_tabPressed = false;
	}

	m_pixelWidth = m_fractalWidth / (double)m_width;

	if(redraw) Redraw();
	if(polyChanged) FindNewZeros();

	// record
	{
		m_recording << "fractal newton -f out/frame" << m_frameCount << ".png";
		m_recording << " -x " << 1920 << " -y " << 1080;
		m_recording << " -i " << 30;
		m_recording << " --n_poly \"[";
		for(uint32_t i = 0; i <= m_order; ++i) {
			if(i > 0) {
				m_recording << " + ";
			}
			m_recording << m_coeffs[i] << "x^" << i;
		}
		m_recording << "]\"";
		m_recording << " --n_center " << m_fractalCenter;
		m_recording << " --n_width " << m_fractalWidth;
		m_recording << " --n_zeros \"";
		for(uint32_t i = 0; i < m_order; ++i) {
			if(i > 0) m_recording << " ";
			m_recording << m_zeros[i];
		}
		m_recording << "\"";
		m_recording << " -c colors.png";
		m_recording << endl;

		++m_frameCount;
	}

	if(m_redraw) {
		m_redraw = false;

		m_nextPixel = 0;

		// build column map and stride map
		if(p_columnMap != nullptr) {
			delete[] p_columnMap;
		}
		if(p_strideMap != nullptr) {
			delete[] p_strideMap;
		}

		uint32_t pow = (uint32_t)ceil(log(max(m_width, m_height))/log(2.0));
		uint32_t initialStride = 1 << pow;
		m_strideCount = pow + 1;

		p_columnMap = new uint32_t[m_width];
		p_strideMap = new uint32_t[m_strideCount];

		p_columnMap[0] = 0;
		p_strideMap[0] = m_height;

		uint32_t currentPixel = m_height;
		uint32_t currentColumn = initialStride / 2;
		uint32_t currentStride = initialStride;
		uint32_t iColumn = 1;
		uint32_t iStride = 1;

		while(true) {
			if(currentColumn >= m_width) {
				p_strideMap[iStride] = currentPixel;
				iStride++;
				currentStride /= 2;
				currentColumn = currentStride / 2;

				if(currentPixel >= m_width * m_height) break;
			}

			p_columnMap[iColumn] = currentColumn;

			currentColumn += currentStride;
			currentPixel += m_height;
			iColumn++;
		}
	}

	// start threads
	for(uint32_t i = 0; i < THREAD_COUNT; ++i) {
		m_threadState[i] = ThreadState::RUN;
	}

	// wait
	this_thread::sleep_for(chrono::milliseconds(16));

	// compute new stride
	uint32_t minPixel = m_width * m_height;
	for(uint32_t i = 0; i < THREAD_COUNT; ++i) {
		minPixel = min(minPixel, (uint32_t)m_currentPixel[i]);
	}
	m_stride = 1;
	for(uint32_t i = 0; i < m_strideCount; ++i) {
		if(minPixel < p_strideMap[i]) {
			if(i > 0) {
				m_stride = (uint32_t)pow(2, m_strideCount - i);
			} else {
				m_stride = 0;
			}
			break;
		}
	}

	// stop threads
	for(uint32_t i = 0; i < THREAD_COUNT; ++i) {
		m_threadState[i] = ThreadState::IDLE;
	}

	// wait for threads to stop
	for(uint32_t i = 0; i < THREAD_COUNT; ++i) {
		while(m_threadState[i] != ThreadState::IDLING);
	}

	// update texture
	glTextureSubImage2D(gl_texture, 0, 0, 0, m_width, m_height, GL_RGB, GL_FLOAT, p_data);
}

void CPUNewton::DrawHUD() {
	stringstream polyString;
	for(int32_t i = m_order; i >= 0; i--) {
		polyString << m_coeffs[i] << "x^" << i << " ";
	}

	Vector2_float pos = {
		-float(o_program->GetWindowWidth()) / 2.0f + 4.0f,
		-float(o_program->GetWindowHeight()) / 2.0f + 20.0f
	};

	o_program->DrawCharacterString(
		polyString.str(),
		pos + Vector2_float{ 2.0f, 2.0f },
		{ 0.0f, 0.0f, 0.0f, 1.0f },
		16.0f,
		{ 6.0f/8.0f, 1.0f }
	);
	o_program->FlushCharacters();
	o_program->DrawCharacterString(
		polyString.str(),
		pos,
		{ 1.0f, 1.0f, 1.0f, 1.0f },
		16.0f,
		{ 6.0f/8.0f, 1.0f }
	);
	o_program->FlushCharacters();
}

void CPUNewton::Resize(uint32_t width, uint32_t height) {
	m_width = width;
	m_height = height;

	// delete old texture
	glBindTexture(GL_TEXTURE_2D, 0);
	glDeleteTextures(1, &gl_texture);

	// create new texture
	glCreateTextures(GL_TEXTURE_2D, 1, &gl_texture);
	glTextureStorage2D(gl_texture, 1, GL_RGB32F, m_width, m_height);
	glBindTexture(GL_TEXTURE_2D, gl_texture);

	GLfloat* data = new GLfloat[3 * width * height];
	for(unsigned y = 0; y < m_height; y++) {
		for(unsigned x = 0; x < m_width; x++) {
			// test values
			data[3 * (y * m_width + x)] = (float)x / (float)m_width;
			data[3 * (y * m_width + x) + 1] = (float)y / (float)m_height;
			data[3 * (y * m_width + x) + 2] = 0.0f;
		}
	}
	glTextureSubImage2D(gl_texture, 0, 0, 0, m_width, m_height, GL_RGB, GL_FLOAT, data);
	delete[] data;

	// delete old buffer
	delete[] p_data;
	p_data = new ColorCvt::RGB<float>[m_width * m_height];

	// redraw
	m_pixelWidth = m_fractalWidth / (double)m_width;
	m_redraw = true;
}

void CPUNewton::ThreadFunction(CPUNewton* n, uint32_t i) {
	complex<double> evalBuffer[MAX_POLY_ORDER + 1];

	while(!n->m_stopRequired) {
		if(n->m_threadState[i] == ThreadState::RUN) {
			n->m_currentPixel[i] = n->m_nextPixel++;
			if(n->m_currentPixel[i] >= n->m_width*n->m_height) {
				n->m_threadState[i] = ThreadState::IDLING;
				continue;
			}
			uint32_t row = n->m_currentPixel[i] % n->m_height;
			uint32_t col = n->p_columnMap[n->m_currentPixel[i] / n->m_height];

			complex<double> z = n->m_fractalCenter + n->m_pixelWidth * complex<double>{
				(double)col - (double)n->m_width/2,
				(double)row - (double)n->m_height/2
			};
			complex<double> old;

			uint32_t iterations = n->NewtonMethod(
				evalBuffer,
				n->m_coeffs,
				n->m_order,
				n->m_maxIterations,
				z,
				ZERO_EPSILON,
				&z
			);
			if(iterations < n->m_maxIterations) {
				for(uint32_t j = 0; j < n->m_order; ++j) {
					if(norm(n->m_zeros[j] - z) < 2.0*ZERO_EPSILON*ZERO_EPSILON) {
						ColorCvt::HSV<float> c = n->m_colors[j + 1];
						c.v *= 1.0f - (float)iterations / (float)n->m_maxIterations;
						n->p_data[row * n->m_width + col] = ColorCvt::HSVtoRGB(c);
						break;
					}
				}
			} else {
				n->p_data[row * n->m_width + col] = ColorCvt::HSVtoRGB(n->m_colors[0]);
			}
		} else if(n->m_threadState[i] == ThreadState::IDLE) {
			n->m_threadState[i] = ThreadState::IDLING;
		} else {
			this_thread::sleep_for(chrono::milliseconds(1));
		}
	}
}

std::complex<double> CPUNewton::EvalRatio(
	complex<double>* evalBuffer,
	complex<double>* coeffs,
	uint32_t order,
	complex<double> value
) {
	std::complex<double> f;
	std::complex<double> f_;

	evalBuffer[0] = 1.0;
	f = coeffs[0];
	f_ = coeffs[1];

	for(uint32_t i = 1; i <= order; i++) {
		evalBuffer[i] = evalBuffer[i - 1] * value;
		f += coeffs[i] * evalBuffer[i];
		if(i < order) {
			f_ += complex<double>(i + 1, 0) * coeffs[i + 1] * evalBuffer[i];
		}
	}

	return f / f_;
}

void CPUNewton::FactorZero(
	complex<double>* coeffs,
	uint32_t order,
	complex<double> zero,
	complex<double>* resCoeffs
) {
	if(order == 0) return;
	complex<double> diff = 0.0;
	for(int32_t i = order - 1; i >= 0; i--) {
		resCoeffs[i] = coeffs[i + 1] - diff;
		diff = -zero * resCoeffs[i];
	}
}

uint32_t CPUNewton::NewtonMethod(
	std::complex<double>* evalBuffer,
	std::complex<double>* coeffs,
	uint32_t order,
	uint32_t maxIter,
	std::complex<double> value,
	double convergence,
	std::complex<double>* resZero
) {
	if(order == 0) return false;
	if(order == 1) {
		*resZero = -coeffs[0] / coeffs[1];
		return true;
	}

	complex<double> old; // far away

	convergence *= convergence; // check against square instead

	for(uint32_t i = 0; i < maxIter; i++) {
		old = value;
		value = value - EvalRatio(evalBuffer, coeffs, order, value);
		if(norm(value - old) < convergence) {
			*resZero = value;
			return i;
		}
	}

	return maxIter;
}

void CPUNewton::FindZerosRandomly() {
	std::complex<double> coeffs1[MAX_POLY_ORDER + 1];
	std::complex<double> coeffs2[MAX_POLY_ORDER];

	std::complex<double>* p = coeffs1;
	std::complex<double>* q = coeffs2;

	memcpy(p, m_coeffs, sizeof(complex<double>) * (MAX_POLY_ORDER + 1));

	complex<double> evalBuffer[MAX_POLY_ORDER + 1];

	for(uint32_t i = 0; i < m_order; i++) {

		if(i > 0) {
			FactorZero(
				p,
				m_order - i + 1,
				m_zeros[i - 1],
				q
			);
			swap(p, q);
		}

		double maxCoeff = 0.0;
		for(uint32_t i = 0; i < m_order; ++i) {
			maxCoeff = max(maxCoeff, abs(m_coeffs[i]));
		}

		while(true) {
			double re = m_randomDistribution(
				m_randomGenerator,
				uniform_real_distribution<double>::param_type{ -maxCoeff, maxCoeff }
			);
			double im = m_randomDistribution(
				m_randomGenerator,
				uniform_real_distribution<double>::param_type{ -maxCoeff, maxCoeff }
			);
			if(NewtonMethod(
				evalBuffer,
				p,
				m_order - i,
				INITIAL_ITERATIONS,
				{ re, im },
				ZERO_EPSILON,
				&m_zeros[i]
			) < INITIAL_ITERATIONS) break;
		}
	}
}

void CPUNewton::FindNewZeros() {
	std::complex<double> coeffs1[MAX_POLY_ORDER + 1];
	std::complex<double> coeffs2[MAX_POLY_ORDER];

	std::complex<double>* p = coeffs1;
	std::complex<double>* q = coeffs2;

	memcpy(p, m_coeffs, sizeof(complex<double>) * (MAX_POLY_ORDER + 1));

	complex<double> evalBuffer[MAX_POLY_ORDER + 1];

	for(uint32_t i = 0; i < m_order; i++) {

		if(i > 0) {
			FactorZero(
				p,
				m_order - i + 1,
				m_zeros[i - 1],
				q
			);
			swap(p, q);
		}

		bool repeat = false;
		while(true) {
			complex<double> z = m_zeros[i];

			if(repeat) {
				double re = m_randomDistribution(
					m_randomGenerator,
					uniform_real_distribution<double>::param_type{ -0.1, 0.1 }
				);
				double im = m_randomDistribution(
					m_randomGenerator,
					uniform_real_distribution<double>::param_type{ -0.1, 0.1 }
				);
				z += complex<double>{re, im};
			}
			if(NewtonMethod(
				evalBuffer,
				p,
				m_order - i,
				INITIAL_ITERATIONS,
				z,
				m_pixelWidth * 2.0,
				&m_zeros[i]
			) < INITIAL_ITERATIONS) break;

			repeat = true;
		}
	}
}

}

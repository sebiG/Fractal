#include "standard.h"

#include <iostream>

#include "program.h"

int main(int argc, char** argv) {

	atexit([]() {
		std::cout << "press ENTER to continue" << std::endl;
		std::cin.get();
	});

	LiveFractal::Program program;

	if(!program.Init()) return -1;
	program.Run();
	program.DeInit();

	return 0;
}
CC = g++
CC_FLAGS = --std=c++17 -x c++ -m64
CC_LINK_FLAGS = -lpthread

BIN_DEBUG = x64/debug
BIN_RELEASE = x64/release

DIR_FRACTAL = Fractal
DIR_LIVE_FRACTAL = LiveFractal
DIR_DAILY_MANDELBROT = DailyMandelbrot

OUT_DEBUG_FRACTAL = $(DIR_FRACTAL)/$(BIN_DEBUG)
OUT_DEBUG_LIVE_FRACTAL = $(DIR_LIVE_FRACTAL)/$(BIN_DEBUG)
OUT_DEBUG_DAILY_MANDELBROT = $(DIR_DAILY_MANDELBROT)/$(BIN_DEBUG)

OUT_RELEASE_FRACTAL = $(DIR_FRACTAL)/$(BIN_RELEASE)
OUT_RELEASE_LIVE_FRACTAL = $(DIR_LIVE_FRACTAL)/$(BIN_RELEASE)
OUT_RELEASE_DAILY_MANDELBROT = $(DIR_DAILY_MANDELBROT)/$(BIN_RELEASE)

OUT_FRACTAL =
OUT_LIVE_FRACTAL =
OUT_DAILY_MANDELBROT =
BIN =
ifeq "$(findstring release, $(MAKECMDGOALS))" ""
	OUT_FRACTAL = $(OUT_DEBUG_FRACTAL)
	OUT_LIVE_FRACTAL = $(OUT_DEBUG_LIVE_FRACTAL)
	OUT_MANDELBROT = $(OUT_DEBUG_DAILY_MANDELBROT)
	BIN = $(BIN_DEBUG)
	CC_FLAGS += -g -Og
	CC_LINKER_FLAGS += -g
else
	OUT_FRACTAL = $(OUT_RELEASE_FRACTAL)
	OUT_LIVE_FRACTAL = $(OUT_RELEASE_LIVE_FRACTAL)
	OUT_MANDELBROT = $(OUT_RELEASE_DAILY_MANDELBROT)
	BIN = $(BIN_RELEASE)
	CC_FLAGS += -O3
endif

INC_GLOB = -I/usr/include -I/usr/local/include
INC_PNGIO = -I~/lib/pngIO
INC_FRACTAL = -I$(DIR_FRACTAL)
INC_LIVE_FRACTAL = -I$(DIR_LIVE_FRACTAL)/src
INC_DAILY_MANDELBROT = -I$(DAILY_MANDELBROT)

LIB_GLOB = -L/usr/lib -L/usr/local/lib

__OBJS_FRACTAL = buddhabrot.cpp.o gradient.cpp.o main.cpp.o mandelbrot.cpp.o polyNewton.cpp.o polynomial.cpp.o
OBJS_FRACTAL = $(patsubst %, $(OUT_FRACTAL)/%, $(__OBJS_FRACTAL))

__OBJS_LIVE_FRACTAL = GL/gl3w.c.o cpuNewton.cpp.o main.cpp.o program.cpp.o
OBJS_LIVE_FRACTAL = $(patsubst %, $(OUT_LIVE_FRACTAL)/%, $(__OBJS_LIVE_FRACTAL))

__OBJS_DAILY_MANDELBROT = main.cpp.o
OBJS_DAILY_MANDELBROT = $(patsubst %, $(OUT_LIVE_MANDELBROT)/%, $(__OBJS_DAILY_MANDELBROT))



# default rule

.PHONY: all
all: Fractal DailyMandelbrot

# Fractal

.PHONY: Fractal
Fractal: create-bin-dir $(OBJS_FRACTAL)
	$(CC) $(CC_LINK_FLAGS) -lpng $(OBJS_FRACTAL) -o $(BIN)/Fractal

$(OUT_FRACTAL)/%.o: $(DIR_FRACTAL)/%
	mkdir -p $(dir $@)
	$(CC) $(CC_FLAGS) -c $(INC_GLOB) $(INC_PNGIO) $(INC_FRACTAL) $(LIB_GLOB) $< -o $@

# Live Fractal
#
#.PHONY: LiveFractal
#LiveFractal: create-bin-dir $(OBJS_LIVE_FRACTAL)
#	$(CC) $(CC_LINK_FLAGS) $(OBJS_LIVE_FRACTAL) -o $(BIN)/LiveFractal
#
#$(OUT_LIVE_FRACTAL)/%.o: $(DIR_LIVE_FRACTAL)/src/%
#	mkdir -p $(dir $@)
#	$(CC) $(CC_FLAGS) -c $(INC_GLOB) $(INC_LIVE_FRACTAL) $(LIB_GLOB) $< -o $@

# Daily Mandelbrot

.PHONY: DailyMandelbrot
DailyMandelbrot: create-bin-dir $(OBJS_DAILY_MANDELBROT)
	$(CC) $(CC_LINK_FLAGS) $(OBJS_DAILY_MANDELBROT) -o $(BIN)/DailyMandelbrot

$(OUT_DAILY_MANDELBROT)/%.o: $(DIR_DAILY_MANDELBROT)/%
	mkdir -p $(dir $@)
	$(CC) $(CC_FLAGS) -c $(INC_GLOB) $(INC_DAILY_MANDELBROT) $(LIB_GLOB) $< -o $@

# creates binary directory

.PHONY: create-bin-dir
create-bin-dir:
	mkdir -p $(BIN)

# cleans up

.PHONY: clean
clean:
	find . -type f -name '*.o' -exec rm {} +











#include "polyNewton.h"

#include <algorithm>
#include <fstream>
#include <thread>

#include <pngIO/pngIO.h>
#include <colorCvt/colorCvt.h>

using namespace std;

namespace Fractal {

PolyNewton::PolyNewton(
	uint32_t imageWidth,
	uint32_t imageHeight,
	uint32_t maxIterations,
	Polynomial const& polynomial,
	complex<double> stepScale,
	std::complex<double> center,
	double width,
	string const& filename,
	uint32_t threadCount
) :
	m_colors({}),

	m_imageWidth(imageWidth),
	m_imageHeight(imageHeight),
	m_pixelCount(imageWidth * imageHeight),
	m_maxIterations(maxIterations),
	m_polynomial(polynomial),
	m_derivative(polynomial.Differentiate()),
	m_center(center),
	m_width(width),
	m_stepScale(stepScale),

	m_zeros({}),

	m_filename(filename),
	m_threadCount(threadCount),

	p_buffers(nullptr),
	m_bufferCount(0)
{
	m_zeros.reserve(m_polynomial.Rank());
}

PolyNewton::~PolyNewton() {
	if(p_buffers != nullptr) {
		for(uint32_t i = 0; i < m_bufferCount; i++) {
			if(p_buffers[i] != nullptr) {
				delete[] p_buffers[i];
			}
		}
		delete[] p_buffers;
	}
}

void PolyNewton::Init() {
	m_bufferCount = m_pixelCount / PIXELS_PER_BUFFER + (m_pixelCount % PIXELS_PER_BUFFER != 0);

	p_buffers = new PixelData*[m_bufferCount];
	memset(p_buffers, 0, m_bufferCount * sizeof(PixelData*));

	for(uint32_t i = 0; i < m_bufferCount; i++) {
		p_buffers[i] = new PixelData[PIXELS_PER_BUFFER];
		memset(p_buffers[i], 0, PIXELS_PER_BUFFER * sizeof(PixelData));
	}
}

void PolyNewton::LoadColors(std::string const& colorFile) {

	ifstream is(colorFile, ios::binary);
	if(!is.is_open()) {
		cout << "Cannot open color file '" << colorFile << "'." << endl;
		return;
	}

	uint32_t width;
	uint32_t height;
	int colorType;
	uint32_t channels;
	uint32_t bitDepth;
	unsigned char* data;

	try {
		PngIO::Read(is, &width, &height, &colorType, &channels, &bitDepth, &data);
	} catch(runtime_error e) {
		cout << e.what() << endl;
	}

	if(bitDepth != 8 || channels < 3) {
		cout << "Unsupported PNG format while reading color file '" << colorFile << "'" << endl;
		return;
	}

	// read colors
	m_colors.clear();
	m_colors.reserve(width);

	for(uint32_t i = 0; i < width; i++) {
		float red = (float)data[i * channels] / 255.0f;
		float green = (float)data[i * channels + 1] / 255.0f;
		float blue = (float)data[i * channels + 2] / 255.0f;
		cout << red << " " << green << " " << blue << endl;
		m_colors.push_back({ red, green, blue });
	}
}

void PolyNewton::LoadZeros(vector<complex<double>> const& zeros) {
	m_zeros = zeros;
}

void PolyNewton::Run() {
	thread* threads = new thread[m_threadCount];

	uint32_t pixelsPerThread = m_pixelCount / m_threadCount + (m_pixelCount % m_threadCount != 0);

	// start threads
	{
		uint32_t pixelsLeft = m_pixelCount;
		for(uint32_t i = 0; i < m_threadCount; i++) {
			threads[i] = thread(ThreadFunction,
				this,
				m_center,
				m_width,
				i,
				m_threadCount
			);
			pixelsLeft -= min(pixelsLeft, pixelsPerThread);
		}
	}

	// join threads
	for(uint32_t i = 0; i < m_threadCount; i++) {
		if(threads[i].joinable()) threads[i].join();
	}
	delete[] threads;

	// Find colours for zeros if not given already
	if(m_colors.empty() && m_zeros.size() > 0) {
		float count = (float)m_zeros.size(); // Use this rather than m_polynomial.Rank()
		float step = 360.0f / count;
		for(uint32_t i = 0; i < m_zeros.size(); i++) {
			m_colors.push_back(ColorCvt::HSVtoRGB(ColorCvt::HSV<>{ step * (float)i, 1.0f, 1.0f }));
		}
	}
	// Repeat colours if neccesary
	if(m_colors.size() < m_zeros.size()) {
		uint32_t oldSize = (uint32_t)m_colors.size();
		for(uint32_t i = (uint32_t)m_colors.size(); i < m_zeros.size(); i++) {
			m_colors.push_back(m_colors[i % oldSize]);
		}
	}

	{
		// start png
		ofstream of(m_filename, ios::binary);
		if(!of.is_open()) {
			cout << "Cannot open file '" << m_filename << "' for writing." << endl;
			return;
		}

		PngIO::PngWriter writer = PngIO::PngWriter(&of, m_imageWidth, m_imageHeight, PNG_COLOR_TYPE_RGB);
		writer.StartPng();

		// write data
		uint8_t* row = new uint8_t[m_imageWidth * 3];
		uint32_t pixelsLeftInRow = m_imageWidth;
		uint32_t pixelsLeft = m_pixelCount;

		for(uint32_t i = 0; i < m_bufferCount; i++) {
			uint32_t pixelsLeftInBuffer = PIXELS_PER_BUFFER;
			while(pixelsLeft > 0 && pixelsLeftInBuffer > 0) {
				uint32_t pixelsToCopy = min(pixelsLeft, min(pixelsLeftInBuffer, pixelsLeftInRow));
				for(uint32_t j = 0; j < pixelsToCopy; j++) {
					PixelData data = p_buffers[i][PIXELS_PER_BUFFER - pixelsLeftInBuffer];
					float value = 1.0f - (float)data.m_iterations / (float)m_maxIterations;
					if(data.m_iterations == m_maxIterations) {
						row[3 * (m_imageWidth - pixelsLeftInRow)] = 0;
						row[3 * (m_imageWidth - pixelsLeftInRow) + 1] = 0;
						row[3 * (m_imageWidth - pixelsLeftInRow) + 2] = 0;
					} else {
						row[3 * (m_imageWidth - pixelsLeftInRow)] = (uint8_t)(m_colors[data.m_colourIndex].r * 255.0f * value);
						row[3 * (m_imageWidth - pixelsLeftInRow) + 1] = (uint8_t)(m_colors[data.m_colourIndex].g * 255.0f * value);
						row[3 * (m_imageWidth - pixelsLeftInRow) + 2] = (uint8_t)(m_colors[data.m_colourIndex].b * 255.0f * value);
					}
					pixelsLeft--;
					pixelsLeftInRow--;
					pixelsLeftInBuffer--;
					if(pixelsLeftInRow == 0) {
						writer.WriteRow((unsigned char*)row);
						pixelsLeftInRow = m_imageWidth;
					}
				}
			}
		}

		// end png
		writer.EndPng();
	}
	
}

void PolyNewton::ThreadFunction(
	PolyNewton* pn,
	complex<double> center,
	double width,
	uint32_t offset,
	uint32_t stride
) {

	// height of the image on imaginary axis
	double height = width * (double)pn->m_imageHeight / (double)pn->m_imageWidth;
	// size of a single pixel (used as convergence criteria)
	double pixelSize = width / (double)pn->m_imageWidth;

	const static double epsilon = 1e-2;

	for(uint32_t i = offset; i < pn->m_pixelCount; i += stride) {
		uint32_t x = i % pn->m_imageWidth;
		uint32_t y = i / pn->m_imageWidth;

		// start value
		complex<double> z = complex<double>((double)x - (double)pn->m_imageWidth / 2.0, (double)y - (double)pn->m_imageHeight / 2.0) * pixelSize + center;

		uint32_t bufferIndex = (y * pn->m_imageWidth + x) / PIXELS_PER_BUFFER;
		uint32_t pixelIndex = (y * pn->m_imageWidth + x) % PIXELS_PER_BUFFER;

		for(uint32_t j = 0; j < pn->m_maxIterations; j++) {
			complex<double> old = z;

			z = z - pn->m_stepScale * (pn->m_polynomial(z)) / (pn->m_derivative(z));

			double diff = abs(z - old);

			// if converged, find zero or create new one
			if(diff < epsilon) {
				lock_guard<mutex> guard(pn->m_mutex);

				bool found = false;
				for(uint32_t k = 0; k < pn->m_zeros.size(); k++) {
					if(abs(pn->m_zeros[k] - z) < 2.0 * epsilon) {
						pn->p_buffers[bufferIndex][pixelIndex] = { k, j };
						found = true;
						break;
					}
				}

				// create new zero
				if(!found && pn->m_zeros.size() < pn->m_polynomial.Rank()) {
					pn->p_buffers[bufferIndex][pixelIndex] = { (uint32_t)pn->m_zeros.size(), j };
					pn->m_zeros.push_back(z);
				}

				// stop iterations
				goto end;
			}
		}

		// divergent
		{
			lock_guard<mutex> guard(pn->m_mutex);
			pn->p_buffers[bufferIndex][pixelIndex] = { 0, pn->m_maxIterations };
		}

	end: continue;
	}
}

}

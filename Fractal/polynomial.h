#ifndef POLYNOMIAL_H
#define POLYNOMIAL_H

#include <iostream>
#include <cstdint>
#include <map>
#include <complex>

namespace Fractal {

class Polynomial {

public:

	Polynomial();
	Polynomial(std::map<uint32_t, std::complex<double>> const& coefficients);
	Polynomial(Polynomial const& p);
	~Polynomial();

	void operator=(Polynomial const& p);

	friend std::ostream& operator<<(std::ostream& os, Polynomial const& p);
	friend std::istream& operator>>(std::istream& is, Polynomial& p);

	const static uint32_t RANK_INF = (uint32_t)-1;

	uint32_t Rank() const;
	std::complex<double> operator()(std::complex<double> x) const;
	Polynomial Differentiate() const;

	// Todo: other operators

private:

	struct Term {
		uint32_t m_power;
		std::complex<double> m_coeff;
	};

	uint32_t m_termCount;
	Term* p_terms;

};

}

#endif
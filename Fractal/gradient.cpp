#include "gradient.h"

#include <iostream>

using namespace std;

using namespace ColorCvt;

namespace Fractal {

void Gradient::SetGradientRGB(
	vector< pair< float, RGB<> > > const& gradient
)
{	
	if (gradient.empty()) return;

	if (abs(gradient.front().first) > numeric_limits<float>::epsilon() * 10) return;
	if (abs(gradient.back().first - 1.0f) > numeric_limits<float>::epsilon() * 10) return;

	if (!is_sorted(
		gradient.begin(), gradient.end(),
		[](auto const& left, auto const& right) {
			return left.first < right.first;
		})
	) return;

	m_gradient = gradient;
}

void Gradient::SetGradientRGB(
	vector< RGB<> > const& colors
)
{
	if (colors.empty()) return;

	vector< pair< float, RGB<> > > gradient;

	if (colors.size() == 1) {
		gradient.push_back({ 0.0f, colors[0] });
	}
	else
	{
		gradient.reserve(colors.size());
		for (unsigned i = 0; i < colors.size(); i++) {
			gradient.push_back({ (float)i / (float)(colors.size() - 1), colors[i] });
		}
	}

	return SetGradientRGB(gradient);
}

RGB<> Gradient::Interpolate(float value, bool useHSV) const
{
	if (m_gradient.size() == 0) return { 0.0f, 0.0f, 0.0f };
	if (m_gradient.size() == 1) return m_gradient.front().second;
	if (value <= 0.0) return m_gradient.front().second;
	if (value >= 1.0) return m_gradient.back().second;

	auto _1 = m_gradient.begin();
	auto _2 = _1 + 1;

	while (_2->first < value && _2 != m_gradient.end()) {
		_1++;
		_2++;
	}

	float t = (value - _1->first) / (_2->first - _1->first);

	if (useHSV)
	{
		HSV<> c1 = RGBtoHSV(_1->second);
		HSV<> c2 = RGBtoHSV(_2->second);

		HSV<> res;

		res.s = (1.0f - t) * c1.s + t * c2.s;
		res.v = (1.0f - t) * c1.v + t * c2.v;

		if (abs(c2.h - c1.h) < 180.0f)
		{
			res.h = (1.0f - t) * c1.h + t * c2.h;
		}
		else
		{
			if (c1.h > c2.h)
			{
				swap(c1, c2);
				t = (1.0f - t);
			}

			float c1h_ = c1.h + 360.0f;
			res.h = (1.0f - t) * c1h_ + t * c2.h;

			if (res.h >= 360.0f) res.h -= 360.0f;
		}

		return HSVtoRGB(res);
	}
	else
	{
		RGB<> c1 = _1->second;
		RGB<> c2 = _2->second;

		return {
			(1.0f - t) * c1.r + t * c2.r,
			(1.0f - t) * c1.g + t * c2.g,
			(1.0f - t) * c1.b + t * c2.b
		};
	}

	
}

}
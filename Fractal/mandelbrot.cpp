// code structure mostly the same as in polyNewton.cpp
// thread function is based on old mandelbrot implementation in C#


#include "mandelbrot.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <thread>

#include <pngIO/pngIO.h>
#include <colorCvt/colorCvt.h>

using namespace std;

using namespace ColorCvt;

namespace Fractal {

Mandelbrot::Mandelbrot(
	uint32_t imageWidth,
	uint32_t imageHeight,
	uint32_t maxIterations,
	complex<double> center,
	double width,
	string const& filename,
	uint32_t threadCount,
	uint32_t gradientScale
) :
	m_gradient({}),
	m_gradientScale(gradientScale),

	m_imageWidth(imageWidth),
	m_imageHeight(imageHeight),
	m_pixelCount(imageWidth* imageHeight),
	m_maxIterations(maxIterations),
	m_center(center),
	m_width(width),

	m_filename(filename),
	m_threadCount(threadCount),

	p_buffers(nullptr),
	m_bufferCount(0)
{

}


Mandelbrot::~Mandelbrot()
{
	if (p_buffers != nullptr) {
		for (uint32_t i = 0; i < m_bufferCount; i++) {
			if (p_buffers[i] != nullptr) {
				delete[] p_buffers[i];
			}
		}
		delete[] p_buffers;
	}
}

void Mandelbrot::Init()
{
	m_bufferCount = m_pixelCount / PIXELS_PER_BUFFER
		+ (m_pixelCount % PIXELS_PER_BUFFER != 0);

	p_buffers = new Color* [m_bufferCount];
	memset(p_buffers, 0, m_bufferCount * sizeof(Color*));

	for (uint32_t i = 0; i < m_bufferCount; i++) {
		p_buffers[i] = new Color[PIXELS_PER_BUFFER];
		memset(p_buffers[i], 0, PIXELS_PER_BUFFER * sizeof(Color));
	}
}

void Mandelbrot::LoadGradient(string const& gradientFile) 
{
	ifstream is(gradientFile, ios::binary);
	if (!is.is_open()) {
		cout << "Cannot open color file '" << gradientFile << "'." << endl;
		return;
	}

	uint32_t width;
	uint32_t height;
	int colorType;
	uint32_t channels;
	uint32_t bitDepth;
	unsigned char* data;

	try {
		PngIO::Read(
			is, &width, &height, &colorType, &channels, &bitDepth, &data
		);
	}
	catch (runtime_error e) {
		cout << e.what() << endl;
	}

	if (bitDepth != 8 || channels < 3) {
		cout << "Unsupported PNG format while reading color file '"
			<< gradientFile << "'" << endl;
		return;
	}

	// read gradient
	vector< RGB<> > colors;
	colors.reserve(width);

	for (uint32_t i = 0; i < width; i++) {
		float red = (float)data[i * channels] / 255.0f;
		float green = (float)data[i * channels + 1] / 255.0f;
		float blue = (float)data[i * channels + 2] / 255.0f;
		colors.push_back({ red, green, blue });
	}

	m_gradient.SetGradientRGB(colors);
}

void Mandelbrot::LoadDefaultGradient_and_Scale() {

	vector< RGB<> > colors = {
		RGB<> {0, 7, 100},
		RGB<> {12, 44, 138},
		RGB<> {24, 82, 177},
		RGB<> {57, 125, 209},
		RGB<> {134, 181, 229},
		RGB<> {211, 236, 248},
		RGB<> {241, 233, 191},
		RGB<> {248, 201, 95},
		RGB<> {255, 170, 0},
		RGB<> {204, 128, 0},
		RGB<> {153, 87, 0},
		RGB<> {66, 30, 15},
		RGB<> {25, 7, 26},
		RGB<> {9, 1, 47},
		RGB<> {4, 4, 73},
		RGB<> {0, 7, 100},
		RGB<> {12, 44, 138},
		RGB<> {24, 82, 177},
		RGB<> {57, 125, 209},
		RGB<> {134, 181, 229},
		RGB<> {211, 236, 248},
		RGB<> {241, 233, 191},
		RGB<> {248, 201, 95},
		RGB<> {255, 170, 0},
		RGB<> {204, 128, 0},
		RGB<> {153, 87, 0},
		RGB<> {66, 30, 15},
		RGB<> {25, 7, 26},
		RGB<> {9, 1, 47},
		RGB<> {4, 4, 73},
		RGB<> {0, 7, 100},
		RGB<> {12, 44, 138},
		RGB<> {24, 82, 177},
		RGB<> {57, 125, 209},
		RGB<> {134, 181, 229},
		RGB<> {211, 236, 248},
		RGB<> {241, 233, 191},
		RGB<> {248, 201, 95},
		RGB<> {255, 170, 0},
		RGB<> {204, 128, 0},
		RGB<> {153, 87, 0},
		RGB<> {66, 30, 15},
		RGB<> {25, 7, 26},
		RGB<> {9, 1, 47},
		RGB<> {4, 4, 73},
		RGB<> {0, 7, 100},
		RGB<> {12, 44, 138},
		RGB<> {24, 82, 177},
		RGB<> {57, 125, 209},
		RGB<> {134, 181, 229},
		RGB<> {211, 236, 248},
		RGB<> {241, 233, 191},
		RGB<> {248, 201, 95},
		RGB<> {255, 170, 0},
		RGB<> {204, 128, 0},
		RGB<> {153, 87, 0},
		RGB<> {66, 30, 15},
		RGB<> {25, 7, 26},
		RGB<> {9, 1, 47},
		RGB<> {4, 4, 73},
		RGB<> {0, 7, 100}
	};
	for (auto& i : colors) {
		i.r /= 256.0;
		i.g /= 256.0;
		i.b /= 256.0;
	}
	m_gradient.SetGradientRGB(colors);

	m_gradientScale = (uint32_t)max((double)m_maxIterations/100.0, 1.0);
}

void Mandelbrot::Run()
{
	thread* threads = new thread[m_threadCount];

	uint32_t pixelsPerThread = m_pixelCount / m_threadCount
		+ (m_pixelCount % m_threadCount != 0);

	// start threads
	{
		uint32_t pixelsLeft = m_pixelCount;
		for (uint32_t i = 0; i < m_threadCount; i++) {
			threads[i] = thread(ThreadFunction,
				this,
				m_center,
				m_width,
				i,
				m_threadCount
			);
			pixelsLeft -= min(pixelsLeft, pixelsPerThread);
		}
	}

	// join threads
	for (uint32_t i = 0; i < m_threadCount; i++) {
		if (threads[i].joinable()) threads[i].join();
	}
	delete[] threads;

	// safe image
	{
		// start png
		ofstream of(m_filename, ios::binary);
		if (!of.is_open()) {
			cout << "Cannot open file '" << m_filename << "' for writing." << endl;
			return;
		}

		PngIO::PngWriter writer = PngIO::PngWriter(
			&of, m_imageWidth, m_imageHeight, PNG_COLOR_TYPE_RGB
		);
		writer.StartPng();

		// write data
		uint8_t* row = new uint8_t[m_imageWidth * 3];
		uint32_t pixelsLeftInRow = m_imageWidth;
		uint32_t pixelsLeft = m_pixelCount;

		for (uint32_t i = 0; i < m_bufferCount; i++) {

			uint32_t pixelsLeftInBuffer = PIXELS_PER_BUFFER;

			while (pixelsLeft > 0 && pixelsLeftInBuffer > 0) {
				uint32_t pixelsToCopy = min(
					pixelsLeft, min(pixelsLeftInBuffer, pixelsLeftInRow)
				);

				for (uint32_t j = 0; j < pixelsToCopy; j++) {

					Color color
						= p_buffers[i][PIXELS_PER_BUFFER - pixelsLeftInBuffer];
					row[3 * (m_imageWidth - pixelsLeftInRow)] = color.r;
					row[3 * (m_imageWidth - pixelsLeftInRow) + 1] = color.g;
					row[3 * (m_imageWidth - pixelsLeftInRow) + 2] = color.b;

					pixelsLeft--;
					pixelsLeftInRow--;
					pixelsLeftInBuffer--;
					if (pixelsLeftInRow == 0) {
						writer.WriteRow((unsigned char*)row);
						pixelsLeftInRow = m_imageWidth;
					}
				}
			}
		}

		// end png
		writer.EndPng();
	}
}

void Mandelbrot::ThreadFunction(
	Mandelbrot* m,
	std::complex<double> center,
	double width,
	uint32_t offset,
	uint32_t stride
)
{
	// height of the image on imaginary axis
	double height = width * (double)m->m_imageHeight / (double)m->m_imageWidth;
	// size of one pixel
	double pixelSize = width / (double)m->m_imageWidth;

	const static double epsilon = 1e-2;

	for (uint32_t i = offset; i < m->m_pixelCount; i += stride) {
		uint32_t x = i % m->m_imageWidth;
		uint32_t y = i / m->m_imageWidth;

		uint32_t bufferIndex = (y * m->m_imageWidth + x) / PIXELS_PER_BUFFER;
		uint32_t pixelIndex = (y * m->m_imageWidth + x) % PIXELS_PER_BUFFER;

		// constant value
		complex<double> c = complex<double>(
			(double)x - (double)m->m_imageWidth / 2.0,
			-(double)y + (double)m->m_imageHeight / 2.0
		) * pixelSize + center;

		// start value
		complex<double> z = 0.0;

		uint32_t j = 0;
		for (; j < m->m_maxIterations; j++)
		{
			if (z.real() * z.real() + z.imag() * z.imag() > 256.0)
			{
				break;
			}

			z = z * z + c;
		}

		// get color
		Color color;
		if (j >= m->m_maxIterations)
		{
			color = { 0, 0, 0 };
		}
		else
		{
			// blend j
			double blended_j = (double)j;
			if (j < m->m_maxIterations)
			{
				double zn = norm(z);
				blended_j += 1.0 - log(log(zn) / log(2.0)) / log(2.0);
			}

			double value = blended_j / (double)m->m_maxIterations;
			value *= (double)m->m_gradientScale;

			value -= floor(value);

			RGB<> c = m->m_gradient.Interpolate((float)value);

			color = {
				(uint8_t)(c.r * 255.0f),
				(uint8_t)(c.g * 255.0f),
				(uint8_t)(c.b * 255.0f)
			};
		}

		// write color
		{
			lock_guard<mutex> guard(m->m_mutex);
			m->p_buffers[bufferIndex][pixelIndex] = color;
		}
	}
}

}

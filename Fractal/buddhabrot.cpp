#include "buddhabrot.h"

#include "math.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <thread>
#include <algorithm>
#include <random>
#include <cmath>

using namespace std;

namespace Fractal {

Buddhabrot::Buddhabrot(
	uint32_t imageWidth,
	uint32_t imageHeight,
	uint32_t iterations,
	uint32_t iterationThreshold,
	uint32_t samples,
	std::string const& filename,
	uint32_t threadCount
) :
	m_imageWidth(imageWidth),
	m_imageHeight(imageHeight),
	m_pixelCount(imageWidth * imageHeight),
	m_iterations(iterations),
	m_iterationThreshold(iterationThreshold),
	m_samples(samples),

	m_filename(filename),
	m_threadCount(threadCount) {
}

Buddhabrot::~Buddhabrot() {
	// Clear buffers

	for(uint32_t i = 0; i < m_bufferCount; i++) {
		delete[] p_buffers[i];
	}

	delete[] p_bufferMutexes;
	delete[] p_buffers;
}

void Buddhabrot::Init() {

	m_bufferCount = m_pixelCount / BUFFER_PIXEL_COUNT + (m_pixelCount % BUFFER_PIXEL_COUNT != 0);
	// = ceil(m_pixelCount / BUFFER_PIXEL_COUNT)

	p_buffers = new float*[m_bufferCount];
	p_bufferMutexes = new mutex[m_bufferCount];

	for(uint32_t i = 0; i < m_bufferCount; i++) {
		p_buffers[i] = new float[BUFFER_PIXEL_COUNT];
		memset(p_buffers[i], 0, BUFFER_PIXEL_COUNT * sizeof(float));
	}

}

void Buddhabrot::ThreadFunction(
	float** buffers,
	std::mutex* mutexes,
	uint32_t iterations,
	uint32_t iterationThreshold,
	uint32_t samples,
	uint32_t imageWidth,
	uint32_t imageHeight,
	double centerX,
	double centerY,
	double width
) {

	// Height of image on the imaginary axis

	double height = width * (double)imageHeight / (double)imageWidth;

	// Random number generator

	mt19937 generator = mt19937(random_device{}());
	uniform_real_distribution<double> distribution;

	// Sample

	for(uint32_t i = 0; i < samples; i++) {

		double r = distribution(generator, uniform_real_distribution<double>::param_type{ 0.0, 2.0 });
		double theta = distribution(generator, uniform_real_distribution<double>::param_type{ 0.0, 2.0 * M_PI });

		double re = r * cos(theta);
		double im = r * sin(theta);

		// Make sure the point is not in the cardioid or the period-2 bulb
		{
			double p = sqrt((re - 0.25)*(re - 0.25) + im * im);
			if(
				re < (p - 2.0 * p * p + 0.25) ||
				(re + 1.0)*(re + 1.0) + im * im < 1.0 / 16.0
				) {
				continue;
			}
		}

		// Do first iteration
		double it = 0.0;
		double re_ = 0.0;
		double im_ = 0.0;

		for(; it < (double)iterations; it += 1.0) {
			if(re_ * re_ + im_ * im_ > 4.0) break;
			double re__ = re_ * re_ - im_ * im_ + re;
			double im__ = 2.0 * re_ * im_ + im;
			re_ = re__;
			im_ = im__;
		}

		if(it < (double)iterations && it >(double)iterationThreshold) {

			// Do second iteration and accumulate
			it = 0.0;
			re_ = 0.0;
			im_ = 0.0;

			for(; it < (double)iterations; it += 1.0) {
				if(re_ * re_ + im_ * im_ > 4.0) break;
				double re__ = re_ * re_ - im_ * im_ + re;
				double im__ = 2.0 * re_ * im_ + im;
				re_ = re__;
				im_ = im__;

				// Accumulate
				int64_t pixelX = (int64_t)round(((re_ - centerX) / width + 0.5) * (double)imageWidth);
				int64_t pixelY = (int64_t)round(((im_ - centerY) / height + 0.5) * (double)imageHeight);
				if(pixelX < 0 || pixelY < 0 || pixelX >= imageWidth || pixelY >= imageHeight) continue;

				int64_t bufferIndex = (pixelY * imageWidth + pixelX) / BUFFER_PIXEL_COUNT;
				int64_t pixelIndex = (pixelY * imageWidth + pixelX) % BUFFER_PIXEL_COUNT;

				{
					lock_guard<mutex> lk(mutexes[bufferIndex]);
					buffers[bufferIndex][pixelIndex] += 1.0f;
				}
			}
		}

	}

}

void Buddhabrot::Run() {

	thread* threads = new thread[m_threadCount];

	uint32_t samplesPerThread = m_samples / m_threadCount + (m_samples % m_threadCount != 0);
	// = ceil(m_samples / m_threadCount)

	// Run threads
	uint32_t samplesLeft = m_samples;
	for(uint32_t i = 0; i < m_threadCount; i++) {
		threads[i] = thread(ThreadFunction,
			p_buffers,
			p_bufferMutexes,
			m_iterations,
			m_iterationThreshold,
			min(samplesLeft, samplesPerThread),
			m_imageWidth,
			m_imageHeight,
			-0.5,
			0.0,
			4.0
		);
		samplesLeft -= min(samplesLeft, samplesPerThread);
	}

	// Join threads
	for(uint32_t i = 0; i < m_threadCount; i++) {
		threads[i].join();
	}

	delete[] threads;

	// Start png

	ofstream of(m_filename, ios::binary);
	if(!of.is_open()) {
		cout << "Cannot open file '" << m_filename << "' for writing." << endl;
		return;
	}

	PngIO::PngWriter writer = PngIO::PngWriter(&of, m_imageWidth, m_imageHeight, PNG_COLOR_TYPE_GRAY);

	writer.StartPng();

	// Find meaningful divider for accumulated values
	float minValue = INFINITY;
	float maxValue = 0.0;
	float average = 0.0f;
	for(uint32_t i = 0; i < m_bufferCount; i++) {
		for(uint32_t j = 0; j < BUFFER_PIXEL_COUNT; j++) {
			if(i * BUFFER_PIXEL_COUNT + j >= m_pixelCount) continue;
			average += p_buffers[i][j];
			if(p_buffers[i][j] > maxValue) maxValue = p_buffers[i][j];
			if(p_buffers[i][j] < minValue) minValue = p_buffers[i][j];
		}
	}
	average /= (float)m_pixelCount;

	float standardDeviation = 0.0f;
	for(uint32_t i = 0; i < m_bufferCount; i++) {
		for(uint32_t j = 0; j < BUFFER_PIXEL_COUNT; j++) {
			if(i * BUFFER_PIXEL_COUNT + j >= m_pixelCount) continue;
			standardDeviation += (p_buffers[i][j] - average) * (p_buffers[i][j] - average);
		}
	}
	standardDeviation /= (float)m_pixelCount;
	standardDeviation = sqrt(standardDeviation);

	float divider = average + 5.0f * standardDeviation;

	// Write data
	uint8_t* row = new uint8_t[m_imageWidth];
	uint32_t pixelsLeftInRow = m_imageWidth;
	uint32_t pixelsLeft = m_pixelCount;

	for(uint32_t i = 0; i < m_bufferCount; i++) {
		uint32_t pixelsLeftInBuffer = BUFFER_PIXEL_COUNT;
		while(pixelsLeft > 0 && pixelsLeftInBuffer > 0) {
			uint32_t pixelsToCopy = min(pixelsLeft, min(pixelsLeftInBuffer, pixelsLeftInRow));
			for(uint32_t j = 0; j < pixelsToCopy; j++) {
				uint8_t pixelData = (uint8_t)min(round(p_buffers[i][BUFFER_PIXEL_COUNT - pixelsLeftInBuffer] * 255.0f / divider), 255.0f);

				row[m_imageWidth - pixelsLeftInRow] = pixelData;
				pixelsLeft--;
				pixelsLeftInRow--;
				pixelsLeftInBuffer--;
				if(pixelsLeftInRow == 0) {
					writer.WriteRow((unsigned char*)row);
					pixelsLeftInRow = m_imageWidth;
				}
			}
		}
	}

	// End png
	writer.EndPng();

}

}
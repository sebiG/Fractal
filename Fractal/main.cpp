#include <iostream>
#include <sstream>
#include <cstdint>
#include <complex>

#include <tclap/CmdLine.h>

#include "buddhabrot.h"
#include "polyNewton.h"
#include "mandelbrot.h"
#include "math.h"

using namespace std;

using namespace Fractal;

const static uint32_t THREAD_COUNT = 8;

std::string PolynomialExample() {
	return "For instance: '[1x^5 + (-2.13)x^3 + (1,1)x]' represents the rank-5 polynomial with factors 1, -2.13 and 1+i.";
}

std::string ComplexExample() {
	return "For instance: 3.14, 1.0, (0.0, 1.0) are the complex numbers 3.14, 1.0 and i.";
}

int main(int argc, char** argv) {

	#if _DEBUG
	atexit([]() {
		cin.get();
	});
	#endif

	TCLAP::CmdLine cmd("Fractal Generator", ' ', "0.1");
	std::vector<std::string> allowed({"buddha", "newton", "mandelbrot"});
	TCLAP::ValuesConstraint<std::string> commands(allowed);
	TCLAP::UnlabeledValueArg<std::string> command("command", "command to execute", true, "", &commands);

	// general
	TCLAP::ValueArg<uint32_t> imageWidth("x", "image_width", "image width", false, 0, "unsigned int");
	TCLAP::ValueArg<uint32_t> imageHeight("y", "image_height", "image height", false, 0, "unsigned int");
	TCLAP::ValueArg<uint32_t> iterations("i", "iterations", "iterations count", false, 0, "unsigned int");
	TCLAP::ValueArg<std::string> filename("f", "file", "output file", false, "", "string");
	TCLAP::ValueArg<uint32_t> threadCount("t", "threads", "thread count", false, 8, "unsigned int");
	TCLAP::ValueArg<std::string> colors("c", "colors", "color file", false, "", "string");

	TCLAP::ValueArg<std::string> center("", "center", "center of fractal", false, "", "complex number");
	TCLAP::ValueArg<double> width("", "width", "width of fractal", false, 0.0, "floating-point number");

	// buddha
	TCLAP::ValueArg<uint32_t> buddhaIterationsThreshold("", "b_it_threshold", "iterations threshold used for buddha", false, 0, "unsigned int");
	TCLAP::ValueArg<uint32_t> buddhaSamples("", "b_samples", "samples used for buddha", false, 0, "unsigned int");

	// newton
	TCLAP::ValueArg<std::string> newtonPolynomial("", "n_poly", "polynomial used for newton", false, "", "polynomial");
	TCLAP::ValueArg<std::string> newtonStepScale("", "n_step", "step scale used for newton", false, "1.0", "complex number");
	TCLAP::ValueArg<std::string> newtonZeros("", "n_zeros", "zeros used for newton", false, "", "list of complex numbers");

	// mandelbrot
	TCLAP::ValueArg<uint32_t> mandelbrotGradientScale("", "m_gradient_scale", "How many times the gradient should be repeated (default 1)", false, 1, "positive int");

	cmd.add(mandelbrotGradientScale);

	cmd.add(newtonPolynomial);
	
	cmd.add(buddhaSamples);
	cmd.add(buddhaIterationsThreshold);
	
	cmd.add(width);
	cmd.add(center);

	cmd.add(colors);
	cmd.add(threadCount);
	cmd.add(filename);
	cmd.add(iterations);
	cmd.add(imageHeight);
	cmd.add(imageWidth);
	cmd.add(command);

	cmd.parse(argc, argv);

	std::string commandValue = command.getValue();
	if(commandValue == "buddha")
	{

		// make sure every required argument is set
		if(
			(!imageWidth.isSet())
			|| (!imageHeight.isSet())
			|| (!iterations.isSet())
			|| (!filename.isSet())
			|| (!buddhaIterationsThreshold.isSet())
			|| (!buddhaSamples.isSet())
		) {
			std::cout << "command 'buddha' requires the following arguments:" << std::endl;
			std::cout << "  -" << imageWidth.getFlag() << " (--" << imageWidth.getName() << ")" << std::endl;
			std::cout << "  -" << imageHeight.getFlag() << " (--" << imageHeight.getName() << ")" << std::endl;
			std::cout << "  -" << iterations.getFlag() << " (--" << iterations.getName() << ")" << std::endl;
			std::cout << "  -" << filename.getFlag() << " (--" << filename.getName() << ")" << std::endl;
			std::cout << "  --" << buddhaIterationsThreshold.getName() << std::endl;
			std::cout << "  --" << buddhaSamples.getName() << std::endl;
		} else {
			Buddhabrot b(
				imageWidth.getValue(),
				imageHeight.getValue(),
				iterations.getValue(),
				buddhaIterationsThreshold.getValue(),
				buddhaSamples.getValue(),
				filename.getValue(),
				threadCount.getValue()
			);

			b.Init();
			b.Run();
		}

	}
	else if(commandValue == "newton")
	{

		// make sure every required argument is set
		if(
			(!imageWidth.isSet())
			|| (!imageHeight.isSet())
			|| (!iterations.isSet())
			|| (!filename.isSet())
			|| (!newtonPolynomial.isSet())
			|| (!center.isSet())
			|| (!width.isSet())
		) {
			std::cout << "command 'newton' requires the following arguments:" << std::endl;
			std::cout << "  -" << imageWidth.getFlag() << " (--" << imageWidth.getName() << ")" << std::endl;
			std::cout << "  -" << imageHeight.getFlag() << " (--" << imageHeight.getName() << ")" << std::endl;
			std::cout << "  -" << iterations.getFlag() << " (--" << iterations.getName() << ")" << std::endl;
			std::cout << "  -" << filename.getFlag() << " (--" << filename.getName() << ")" << std::endl;
			std::cout << "  --" << newtonPolynomial.getName() << std::endl;
			std::cout << "  --" << center.getName() << std::endl;
			std::cout << "  --" << width.getName() << std::endl;
		} else {

			Polynomial p;
			std::complex<double> centerValue, stepScale;
			std::vector<std::complex<double>> zeros;

			// Read special values
			{
				std::istringstream iss;

				// polynomial
				iss.clear();
				iss.str(newtonPolynomial.getValue());
				iss >> p;
				if(!iss) {
					cout << "error reading '--n_poly'. " << PolynomialExample() << std::endl;
					return 1;
				}

				// center
				iss.clear();
				iss.str(center.getValue());
				iss >> centerValue;
				if(!iss) {
					cout << "error reading '--n_center'. " << ComplexExample() << std::endl;
					return 1;
				}

				// stepScale
				iss.clear();
				iss.str(newtonStepScale.getValue());
				iss >> stepScale;
				if(!iss) {
					cout << "error reading '--n_step'. " << ComplexExample() << std::endl;
					return 1;
				}

				// zeros
				if(newtonZeros.isSet()) {
					iss.str(newtonZeros.getValue());
					for(uint32_t i = 0; i < p.Rank(); ++i) {
						iss.clear();
						std::complex<double> z;
						iss >> z;
						if(!iss) {
							cout << "error reading '--n_zeros'. " << ComplexExample() << std::endl;
							return 1;
						}
						zeros.push_back(z);
					}
				}
			}

			PolyNewton pn(
				imageWidth.getValue(),
				imageHeight.getValue(),
				iterations.getValue(),
				p,
				stepScale,
				centerValue,
				width.getValue(),
				filename.getValue(),
				threadCount.getValue()
			);

			if(colors.isSet()) {
				pn.LoadColors(colors.getValue());
			}

			if(newtonZeros.isSet()) {
				pn.LoadZeros(zeros);
			}

			pn.Init();
			pn.Run();
		}
	}
	else if (commandValue == "mandelbrot")
	{
		if (
			(!imageWidth.isSet())
			|| (!imageHeight.isSet())
			|| (!iterations.isSet())
			|| (!filename.isSet())
			|| (!center.isSet())
			|| (!width.isSet())
		) {
			std::cout << "command 'mandelbrot' requires the following arguments:" << std::endl;
			std::cout << "  -" << imageWidth.getFlag() << " (--" << imageWidth.getName() << ")" << std::endl;
			std::cout << "  -" << imageHeight.getFlag() << " (--" << imageHeight.getName() << ")" << std::endl;
			std::cout << "  -" << iterations.getFlag() << " (--" << iterations.getName() << ")" << std::endl;
			std::cout << "  -" << filename.getFlag() << " (--" << filename.getName() << ")" << std::endl;
			std::cout << "  --" << center.getName() << std::endl;
			std::cout << "  --" << width.getName() << std::endl;
		}
		else
		{
			std::complex<double> centerValue;
			uint32_t gradientScale;

			// Read special values
			{
				std::istringstream iss;

				// center
				iss.clear();
				iss.str(center.getValue());
				iss >> centerValue;
				if (!iss) {
					cout << "error reading '--n_center'. " << ComplexExample() << std::endl;
					return 1;
				}

				// gradient scale
				gradientScale = mandelbrotGradientScale.getValue();
				if (gradientScale == 0) {
					cout << "'--m_gradient_scale' cannoit be zero." << std::endl;
					return 1;
				}
			}

			Mandelbrot mb(
				imageWidth.getValue(),
				imageHeight.getValue(),
				iterations.getValue(),
				centerValue,
				width.getValue(),
				filename.getValue(),
				threadCount.getValue(),
				mandelbrotGradientScale.getValue()
			);

			if (colors.isSet())
			{
				mb.LoadGradient(colors.getValue());
			}
			else
			{
				mb.LoadDefaultGradient_and_Scale();
			}

			mb.Init();
			mb.Run();
		}
	}

	return 0;
}

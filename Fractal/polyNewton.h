#ifndef POLYNEWTON_H
#define POLYNEWTON_H

#include <cstdint>
#include <string>
#include <mutex>
#include <vector>
#include <complex>

#include <colorCvt/colorCvt.h>

#include "polynomial.h"

namespace Fractal {

class PolyNewton {

public:

	PolyNewton(
		uint32_t imageWidth,
		uint32_t imageHeight,
		uint32_t maxIterations,
		Polynomial const& polynomial,
		std::complex<double> stepScale,
		std::complex<double> center,
		double width,
		std::string const& filename,
		uint32_t threadCount
	);
	~PolyNewton();

	struct PixelData {
		uint32_t m_colourIndex;
		uint32_t m_iterations;
	};

	const static uint32_t BUFFER_SIZE = 1024 * 1024; // 1 MB
	const static uint32_t PIXELS_PER_BUFFER = BUFFER_SIZE / sizeof(PixelData);

	void Init();
	void LoadColors(std::string const& colorFile);
	void LoadZeros(std::vector<std::complex<double>> const& zeros);
	void Run();

	std::vector<ColorCvt::RGB<>> m_colors;

private:

	static void ThreadFunction(
		PolyNewton* pn,
		std::complex<double> center,
		double width,
		uint32_t offset,
		uint32_t stride
	);

	uint32_t m_imageWidth;
	uint32_t m_imageHeight;
	uint32_t m_pixelCount;
	uint32_t m_maxIterations;
	Polynomial m_polynomial;
	Polynomial m_derivative;
	std::complex<double> m_center;
	double m_width;
	std::complex<double> m_stepScale;

	std::vector<std::complex<double>> m_zeros;
	std::mutex m_mutex;

	std::string m_filename;
	uint32_t m_threadCount;

	PixelData** p_buffers;
	uint32_t m_bufferCount;
};

}

#endif
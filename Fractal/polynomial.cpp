#include "polynomial.h"

#include <cstring>

using namespace std;

namespace Fractal {

Polynomial::Polynomial() :
	m_termCount(0),
	p_terms(nullptr) 
{}

Polynomial::Polynomial(map<uint32_t, complex<double>> const& coefficients) : Polynomial() {
	m_termCount = (uint32_t)coefficients.size();
	p_terms = new Term[m_termCount];
	uint32_t i = 0;
	for(auto it = coefficients.rbegin(); it != coefficients.rend(); it++) {
		p_terms[i] = { it->first, it->second };
		i++;
	}
}

Polynomial::Polynomial(Polynomial const& p) : Polynomial() {
	this->operator=(p);
}

Polynomial::~Polynomial() {
	if(p_terms != nullptr) {
		delete[] p_terms;
	}
}

void Polynomial::operator=(Polynomial const& p) {
	this->~Polynomial(); // destruct

	// set new data
	m_termCount = p.m_termCount;
	p_terms = new Term[m_termCount];
	memcpy(p_terms, p.p_terms, m_termCount * sizeof(Term));
}

ostream& operator<<(ostream& os, Polynomial const& p) {
	auto print = [&os](Polynomial::Term t) {
		if(abs(t.m_coeff.imag()) < numeric_limits<double>::epsilon()) {
			os << t.m_coeff.real();
		} else {
			os << t.m_coeff;
		}
		if(t.m_power == 1) {
			os << "x";
		} else if(t.m_power > 1) {
			os << "x^" << t.m_power;
		}
	};
	
	if(p.m_termCount == 0) {
		os << "[0]";
	} else {
		os << "[";
		bool first = true;
		for(uint32_t i = 0; i < p.m_termCount; i++) {
			if(first) {
				first = false;
			} else {
				os << " + ";
			}

			print(p.p_terms[i]);
		}
		os << "]";
	}

	return os;
}

istream& operator>>(istream& is, Polynomial& p) {
	std::map<uint32_t, std::complex<double>> coefficients;

	{ // prevent goto warnings	
	auto ignoreSpaces = [&is]() {
		if(isspace(is.peek())) {
			is.get();
			return true;
		} else {
			return false;
		}
	};

	if(is.peek() != '[') {
		is.setstate(ios::failbit);
		goto end;
	}
	is.get();

	bool first = true;
	do {
		while(ignoreSpaces());
		
		if(first) {
			first = false;
		} else {
			if(is.peek() != '+') goto fail;
			is.get();
			while(ignoreSpaces());
		}

		uint32_t power;
		complex<double> coeff;
		
		if(!(is >> coeff)) goto fail;

		if(is.peek() != 'x') {
			power = 0;
		} else {
			is.get();
			if(is.peek() != '^') {
				power = 1;
			} else {
				is.get();
				if(!(is >> power)) goto fail;
			}
		}

		if(coefficients.find(power) != coefficients.end()) goto fail;

		coefficients.insert({power, coeff});

		while(ignoreSpaces());
	} while(is.peek() != ']');

	// create polynomial with values
	p = Polynomial(coefficients);

	goto end;
	}

fail:
	is.setstate(ios::failbit);
end:

	return is;
}

uint32_t Polynomial::Rank() const {
	if(m_termCount == 0) return RANK_INF;
	return p_terms[0].m_power;
}

complex<double> Polynomial::operator()(complex<double> x) const {
	complex<double> res = 0;

	complex<double> x_ = x;

	uint32_t power = 1;
	for(int32_t i = m_termCount - 1; i >= 0; i--) {
		if(p_terms[i].m_power == 0) {
			res += p_terms[i].m_coeff;
			continue;
		}
		while(power < p_terms[i].m_power) {
			x_ *= x;
			power++;
		}
		res += p_terms[i].m_coeff * x_;
	}

	return res;
}

Polynomial Polynomial::Differentiate() const {
	Polynomial p;

	p.m_termCount = m_termCount - 1;
	if(p.m_termCount > 0) {
		p.p_terms = new Term[p.m_termCount];
		for(uint32_t i = 0; i < p.m_termCount; i++) {
			p.p_terms[i] = { p_terms[i].m_power - 1, (double)p_terms[i].m_power * p_terms[i].m_coeff };
		}
	}

	return p;
}

}

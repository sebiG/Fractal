#ifndef MATH_H
#define MATH_H

#define M_PI 3.1415926535897932384626433832795
#define M_E 2.7182818284590452353602874713527

inline uint64_t Factorial(uint64_t x) {
	uint64_t ret = 1;
	for(uint64_t i = 2; i <= x; i++) {
		ret *= i;
	}
	return ret;
}

#endif
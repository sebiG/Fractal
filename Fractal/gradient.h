#ifndef GRADIENT_H
#define GRADIENT_H

#include <vector>

#include<colorCvt/colorCvt.h>

namespace Fractal
{
	
class Gradient
{
public:

	void SetGradientRGB(
		std::vector< std::pair< float, ColorCvt::RGB<> > > const& gradient
	);

	void SetGradientRGB(
		std::vector< ColorCvt::RGB<> > const& colors
	);

	ColorCvt::RGB<> Interpolate(float value, bool useHSV = false) const;

	std::vector< std::pair< float, ColorCvt::RGB<> > > m_gradient;

};

}

#endif

#ifndef MANDELBROT_H
#define MANDELBROT_H

#include <cstdint>
#include <string>
#include <mutex>
#include <vector>
#include <complex>

#include "gradient.h"

namespace Fractal
{

class Mandelbrot
{
public:

	Mandelbrot(
		uint32_t imageWidth,
		uint32_t imageHeight,
		uint32_t maxIterations,
		std::complex<double> center,
		double width,
		std::string const& filename,
		uint32_t threadCount,
		uint32_t gradientScale
	);
	~Mandelbrot();

	struct Color {
		uint8_t r;
		uint8_t g;
		uint8_t b;
	};

	const static uint32_t BUFFER_SIZE = 1024 * 1024;
	const static uint32_t PIXELS_PER_BUFFER
		= BUFFER_SIZE / sizeof(Color);

	void Init();
	void LoadGradient(std::string const& gradientFile);
	void LoadDefaultGradient_and_Scale();
	void Run();

private:

	static void ThreadFunction(
		Mandelbrot* mandelbrot,
		std::complex<double> center,
		double width,
		uint32_t offset,
		uint32_t stride
	);

	Gradient m_gradient;
	uint32_t m_gradientScale;

	uint32_t m_imageWidth;
	uint32_t m_imageHeight;
	uint32_t m_pixelCount;
	uint32_t m_maxIterations;
	std::complex<double> m_center;
	double m_width;

	std::mutex m_mutex;

	std::string m_filename;
	uint32_t m_threadCount;

	Color** p_buffers;
	uint32_t m_bufferCount;

};

}

#endif
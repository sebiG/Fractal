#ifndef BUDDHABROT_H
#define BUDDHABROT_H

#include <cstdint>
#include <string>
#include <mutex>

#include <pngIO/pngIO.h>

namespace Fractal {

class Buddhabrot {

public:

	Buddhabrot(
		uint32_t imageWidth,
		uint32_t imageHeight,
		uint32_t iterations,
		uint32_t iterationThreshold,
		uint32_t samples,
		std::string const& filename,
		uint32_t threadCount = 8
	);
	~Buddhabrot();

	const static uint32_t BUFFER_SIZE = 1024 * 1024; // 1 MB
	const static uint32_t BUFFER_PIXEL_COUNT = BUFFER_SIZE / sizeof(float); // 1/4 MP

	void Init();

	void Run();

private:

	static void ThreadFunction(
		float** buffers,
		std::mutex* mutexes,
		uint32_t iterations,
		uint32_t iterationThreshold,
		uint32_t samples,
		uint32_t imageWidth,
		uint32_t imageHeight,
		double centerX,
		double centerY,
		double width
	);

	uint32_t m_imageWidth;
	uint32_t m_imageHeight;
	uint32_t m_pixelCount;
	uint32_t m_iterations;
	uint32_t m_iterationThreshold;
	uint32_t m_samples;

	std::string m_filename;
	uint32_t m_threadCount;

	float** p_buffers;
	std::mutex* p_bufferMutexes;
	uint32_t m_bufferCount;

};

}

#endif
#include <iostream>
#include <filesystem>
#include <algorithm>
#include <vector>
#include <complex>
#include <string>
#include <chrono>
#include <fstream>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#define USE_UI

#ifdef USE_UI
#include <opencv2/highgui/highgui.hpp>
#endif

using namespace std;

// different names for linux and windows executables
#ifdef _WIN32
const string programName = "Fractal.exe";
#endif
#ifdef __linux__
const string programName = "./Fractal";
#endif

// functions

void RenderBW(
	double width,
	complex<double> center,
	string const& filename
);

void RenderFrame(
	double width,
	complex<double> center,
	string const& outputFolder,
	string const& gradientFile
);

void RenderZoom(
	double startWidth,
	double endWidth,
	complex<double> center,
	string const& outputFolder,
	string const& gradientFile
);

string GetTimestamp() {
	ostringstream oss;
	auto in_time_t = chrono::system_clock::to_time_t(chrono::system_clock::now());
	oss << std::put_time(std::localtime(&in_time_t), "%F_%H-%M-%S");
	return oss.str();
}

// some constants

const double scaleFactor = 1e-1;
const uint32_t maxZoomSteps = 11;

const uint32_t iterations = 1000;

const uint32_t workingWidth = 1000;
const uint32_t workingHeight = 1000;

const uint32_t finalWidth = 4000;
const uint32_t finalHeight = 4000;

const uint32_t frameCount = 300; // 10 s

enum class Mode {
	FRAME,
	ZOOM
};

int main(int argc, char** argv) {

	Mode mode = Mode::FRAME;

	// switch working mode
	if (argc > 1)
	{
		string modeStr = string(argv[1]);
		if (modeStr == "frame") mode = Mode::FRAME;
		else if (modeStr == "zoom") mode = Mode::ZOOM;
	}

	// get gradient directory
	string gradientFolder = "./gradients";
	if (argc > 2)
	{
		gradientFolder = string(argv[2]);
	}

	// seed random number generator
	srand(time(NULL));

	// set up folders
	filesystem::remove_all("./daily_temp");
	filesystem::create_directory("./daily_temp");
	string outputFolder = "./daily/" + GetTimestamp();
	filesystem::create_directories(outputFolder);

	// get random gradient for fractal
	string gradientFile = "";
	{
		auto _ = filesystem::directory_iterator(gradientFolder);

		vector<filesystem::path> gradientFiles;
		for (auto& entry : _)
		{
			if (entry.is_regular_file() && entry.path().extension() == ".png")
			{
				gradientFiles.push_back(entry.path());
			}
		}

		if (!gradientFiles.empty())
		{
			gradientFile = gradientFiles[rand() % gradientFiles.size()].string();
		}
	}

	uint32_t zoomSteps = 0;
	if (mode == Mode::ZOOM) zoomSteps = maxZoomSteps;
	else if (mode == Mode::FRAME) zoomSteps = rand() % (maxZoomSteps - 1) + 1;

	// working variables
	double width = 3.0;
	double height = width / (double)workingWidth * (double)workingHeight;
	complex<double> center = -0.5;

	// create initial image
	RenderBW(width, center, "./daily_temp/0.png");

#ifdef USE_UI
	// add ui
	cv::namedWindow("last_render");
	cv::namedWindow("edges");
#endif

	for (unsigned i = 0; i <= zoomSteps; i++)
	{
		// detect edges using opencv
		cv::Mat lastRender;
		{
			stringstream lastFile;
			lastFile << "./daily_temp/" << i << ".png";
			lastRender = cv::imread(lastFile.str());
		}

		cv::Mat edges;
		cv::Canny(lastRender, edges, 200, 400);

		// select a random white pixel in the edge image
		uint32_t whitePixelCount = cv::countNonZero(edges);
		if (whitePixelCount == 0) cout << "no white pixels!" << endl;
		uint32_t whitePixelIndex = rand() % whitePixelCount;
		uint32_t nextX, nextY;
		for (nextY = 0; nextY < workingHeight; nextY++)
		{
			for (nextX = 0; nextX < workingWidth; nextX++)
			{
				if (edges.at<uchar>(cv::Point(nextX, nextY)) != 0) // being paranoid here
				{
					if (whitePixelIndex == 0)
					{
						goto endLoop;
					}
					whitePixelIndex--;
				}
			}
		}
	endLoop:;

		cout << nextX << " " << nextY << endl;

#ifdef USE_UI
		cv::cvtColor(edges, edges, cv::COLOR_GRAY2BGR, CV_8U);
		cv::drawMarker(edges, cv::Point(nextX, nextY), { 0, 0, 255 }, cv::MARKER_SQUARE);

		cv::imshow("last_render", lastRender);
		cv::imshow("edges", edges);

		cv::waitKey(0);
#endif

		// find new center
		center += complex<double>(
			-width / 2,
			height / 2
		);
		center += complex<double>(
			width * (double)nextX / (double)workingWidth,
			-height * (double)nextY / (double)workingHeight
		);

		// adjust width
		width *= scaleFactor;
		height = width / (double)workingWidth * (double)workingHeight;

		// render next image
		{
			ostringstream oss;
			oss << "./daily_temp/" << i + 1 << ".png";
			RenderBW(width, center, oss.str());
		}
	}

	// once a position is found, render the actual images
	if (mode == Mode::FRAME)
	{
		RenderFrame(width, center, outputFolder, gradientFile);
	}
	else
	{
		RenderZoom(4.0, width, center, outputFolder, gradientFile);
	}

	return 0;
}

void RenderBW(
	double width,
	complex<double> center,
	string const& filename
)
{

	ostringstream cmd;
	cmd.precision(15);
	cmd << programName;
	cmd << " mandelbrot";
	cmd << " -x " << workingWidth << " -y " << workingHeight;
	cmd << " -i " << iterations;
	cmd << " -f " << filename;
	cmd << " --center \"(" << center.real() << "," << center.imag() << ")\"";
	cmd << " --width " << width;
	cmd << " -c gradient_bw.png";
	cmd << " --m_gradient_scale " << iterations / 10;

	cout << cmd.str() << endl;

	system(cmd.str().c_str());

}

void RenderFrame(
	double width,
	complex<double> center,
	string const& outputFolder,
	string const& gradientFile
) {

	ostringstream cmd;
	cmd.precision(15);
	cmd << programName;
	cmd << " mandelbrot";
	cmd << " -x " << finalWidth << " -y " << finalHeight;
	cmd << " -i " << iterations;
	cmd << " -f " << outputFolder << "/frame.png";
	cmd << " --center \"(" << center.real() << "," << center.imag() << ")\"";
	cmd << " --width " << width;
	if (!gradientFile.empty())
	{
		cmd << " -c " << gradientFile;
	}
	cmd << " --m_gradient_scale " << iterations / 100;

	cout << cmd.str() << endl;

	ofstream os(outputFolder + "/cmdline.txt");
	os << cmd.str() << endl;

	system(cmd.str().c_str());

}

void RenderZoom(
	double startWidth,
	double endWidth,
	complex<double> center,
	string const& outputFolder,
	string const& gradientFile
)
{

	double scaleFactor = pow(endWidth / startWidth, 1.0 / (double)frameCount);

	ofstream of(outputFolder + "/cmdlines.txt");
	
	double width = startWidth;
	for (unsigned i = 0; i < frameCount; i++)
	{

		ostringstream cmd;
		cmd.precision(15);
		cmd << programName;
		cmd << " mandelbrot";
		cmd << " -x " << finalWidth << " -y " << finalHeight;
		cmd << " -i " << iterations;
		cmd << " -f " << outputFolder << "/" << i << ".png";
		cmd << " --center \"(" << center.real() << "," << center.imag() << ")\"";
		cmd << " --width " << width;
		if (!gradientFile.empty())
		{
			cmd << " -c " << gradientFile;
		}
		cmd << " --m_gradient_scale " << iterations / 100;

		cout << cmd.str() << endl;
		of << cmd.str() << endl;

		system(cmd.str().c_str());

		width *= scaleFactor;
	}

}





